module.exports = {
    database: {
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'evo-tree',
        options: {
            host: 'localhost',
        }
    },
    redis: {
        port: 6379,
        host: 'localhost',
        options: {}
    },
    acl: {
        prefix: 'acl'
    }
}
