module.exports = {
    database: {
        host: 'localhost',
        user: null,
        password: null,
        database: 'evo-tree',
        options: {
            dialect: 'sqlite',
            storage: 'test/db',
            // logging: console.log
            logging: false
        }
    },
    acl: {
        prefix: 'aclTest'
    }
}
