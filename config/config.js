var params, aclBackend,
    container = {},
    express = require('express'),
    Sequelize = require('sequelize'),
    mysql = require('mysql'),
    redis = require('redis'),
    acl = require('acl'),
    app = express(),
    env = app.get('env'),
    _ = require('lodash'),
    prod = require('./parametersProduction.js'),
    dev = require('./parametersDevelopment.js'),
    test = require('./parametersTest.js');

// Generate the parameters object
if (env === 'development') {
    params = _.extend({}, prod, dev);
} else if (env === 'test') {
    params = _.extend({}, prod, test);
} else {
    params = prod;
}

// Setup services in container
container.sequelize = new Sequelize(
    params.database.database,
    params.database.user,
    params.database.password,
    params.database.options
);

container.mysql = mysql.createConnection({
    host: params.database.host,
    user: params.database.user,
    password: params.database.password,
    database: params.database.database
});

if (env === 'test') {
    container.acl = new acl(new acl.memoryBackend());
} else {
    container.redis = redis.createClient(params.redis.port, params.redis.host, params.redis.options);
    container.acl = new acl(new acl.redisBackend(container.redis, params.acl.prefix));
}

/**
 * Return a configuration parameter
 */
module.exports.getParam = function (param) {
    if (! params[param]) {
        throw new Error('Configuration parameter ' + param + ' not found.')
    }

    return params[param];
}

/**
 * Return a DI container service
 */
module.exports.get = function (service) {
    if (! container[service]) {
        throw new Error('Service ' + service + ' not found.')
    }

    return container[service];
}
