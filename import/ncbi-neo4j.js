/**
 * Import script for Genbank's taxonomy database
 */

var fs = require('fs'),
    readline = require('readline'),
    stream = require('stream'),
    _ = require('lodash'),
    http = require('http'),
    prettyjson = require('prettyjson');

var dir = '/Users/simon/Downloads/taxdump/',
    nodesFile = dir + 'nodes.dmp',
    namesFile = dir + 'names.dmp',
    nodes = fs.readFileSync(nodesFile).toString().split("\n"),
    names = fs.readFileSync(namesFile).toString().split("\n");
    nodesLength = nodes.length,
    namesLength = names.length,
    batchSize = 10;

//
// String functions
// ================
//
String.prototype.toCamelCase = function () {
    var parts = this.toLowerCase().trim().split(/\s|-/),
        camelCasedString = '';

    for (var i = 0, len = parts.length; i < len; i++) {
        camelCasedString += i === 0 ? parts[i] : parts[i].ucFirst();
    }

    return camelCasedString;
}

String.prototype.ucFirst = function () {
    return this[0].toUpperCase() + this.slice(1);
}

//
// Create Indexes
// ==============
//
var batch = [];
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(rank)'}));
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(genbankTaxId)'}));
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(scientificNames)'}));
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(commonNames)'}));
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(genbankCommonNames)'}));
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(genbankSynonymNames)'}));
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(synonymNames)'}));
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(authorityNames)'}));
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(inPartNames)'}));
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(blastNames)'}));
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(misnomerNames)'}));
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(misspellingNames)'}));
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(typeMaterialNames)'}));
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(includesNames)'}));
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(equivalentNames)'}));
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(anamorphNames)'}));
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(genbankAnamorphNames)'}));
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(teleomorphNames)'}));
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(acronymNames)'}));
batch.push(createBatchItem({query: 'CREATE INDEX ON :Taxa(genbankAcronymNames)'}));
doQueryRequest(batch);

// Kickoff creating nodes
createNodesBatch(0);
// createNamesBatch(0);
// createNamesBatch(1328969);

//
// Create Nodes
// ============
//
function createNodesBatch (offset) {
    var batch = [],
        callback = function (retry) {
            if (retry) {
                createNodesBatch(offset);
            } else if (offset > nodesLength) {
                createRelationshipsBatch(0);
            } else {
                createNodesBatch(offset + batchSize + 1);
            }
        };

    nodes.slice(offset, offset + batchSize).forEach(function (line) {
        var parsed = parseLine(line);

        // Must be on last line of file
        if (parsed.length < 2) {
            createRelationshipsBatch(0);
            return;
        }

        var query = 'CREATE (n:Taxa:'+parsed[2].toCamelCase()+' {' +
                'genbankTaxId: {genbankTaxId}, ' +
                'rank: {rank}' +
            '}) RETURN n',
            params = {
                genbankTaxId: parseInt(parsed[0]),
                rank: parsed[2],
            },
            bodyObject = {
                query: query,
                params: params
            };

        console.log('Create Node ' + parsed[0]);
        batch.push(createBatchItem(bodyObject));
    });

    doQueryRequest(batch, callback);
}

//
// Create Relationships
// ====================
//
function createRelationshipsBatch (offset) {
    var batch = [],
        callback = function (retry) {
            if (retry) {
                createRelationshipsBatch(offset);
            } else if (offset > nodesLength) {
                console.log('Relationships complete.');
                createNamesBatch(0);
            } else {
                createRelationshipsBatch(offset + batchSize + 1);
            }
        };

    nodes.slice(offset, offset + batchSize).forEach(function (line) {
        var parsed = parseLine(line);

        // Must be on last line of file
        if (parsed.length < 2) {
            createNamesBatch(0);
            return;
        }

        var query = 'MATCH (a:Taxa), (b:Taxa) WHERE a.genbankTaxId={id} AND b.genbankTaxId={pId} ' +
                    'CREATE (b)-[:parent_of]->(a) ' +
                    'RETURN a',
            params = {
                id: parseInt(parsed[0]),
                pId: parseInt(parsed[1])
            },
            bodyObject = {
                query: query,
                params: params
            };

        console.log('Create Relationships for Node ' + parsed[0]);
        batch.push(createBatchItem(bodyObject));
    });

    doQueryRequest(batch, callback);
}

//
// Create Names
// ============
//
function createNamesBatch (offset) {
    var query, parsed,
        batch = [],
        queryParts = [],
        currentId = null,
        lineCount = 0,
        namesHash = getNamesHash(),
        newNames = names.slice(offset),
        key = '',
        callback = function (retry) {
            if (retry) {
                createNamesBatch(offset);
            } else if (offset > namesLength) {
                console.log('Names complete.');
                return;
            } else {
                createNamesBatch(offset + lineCount);
            }
        };

    // Don't batch the names as multiple lines exist for the same
    // node. Batching will split up names for the node and fuck up the query.
    for (var i = 0, len = newNames.length; i < len; i++) {
        parsed = parseLine(newNames[i]);

        if (parsed.length < 2) {
            console.log('End of file');
            return;
        }

        if (!currentId) currentId = parsed[0];

        // Either push more names onto the stack or do the
        // query if there are no more names for this node.
        if (parsed[0] === currentId) {
            key = parsed[3].toCamelCase();
            console.log(key);
            namesHash[key].push(parsed[1].replace(/\s\s+|\"/g, '').trim());
        } else {
            _.each(namesHash, function (value, key) {
                if (! value.length) {
                    delete namesHash[key];
                }
            });

            query = 'MATCH (a:Taxa) WHERE a.genbankTaxId={id} SET ';

            if (! _.isEmpty(namesHash)) {
                _.each(namesHash, function (value, key) {
                    queryParts.push('a.'+key+' = {'+key+'}');
                });

                query += queryParts.join(', ') + ' RETURN a';

                params = _.extend({id: parseInt(currentId)}, namesHash),
                bodyObject = {
                    query: query,
                    params: params
                };

                console.log('Create Names for Node ' + parsed[0]);
                namesHash = getNamesHash();
                doQueryRequest([createBatchItem(bodyObject)], callback);
            }

            break;
        }

        lineCount++;
    }
}

function getNamesHash () {
    return {
        scientificName: [],
        commonName: [],
        genbankCommonName: [],
        genbankSynonym: [],
        synonym: [],
        authority: [],
        inPart: [],
        blastName: [],
        misnomer: [],
        misspelling: [],
        typeMaterial: [],
        includes: [],
        equivalentName: [],
        anamorph: [],
        genbankAnamorph: [],
        teleomorph: [],
        acronym: [],
        genbankAcronym: []
    }
}

//
// Functions
// =========
//

function createBatchItem (body) {
    return {
        method: 'POST',
        to: '/cypher',
        body: body
    };
}

function stringifyNames (names) {
    for (type in names) {

    }
}

function parseLine (line) {
    var data = [];

    line.split('|').forEach(function (part) {
        data.push(part.replace(/\t/g, ''));
    });

    return data;
}

function doQueryRequest(object, callback) {
    var options = {
        host: 'localhost',
        port: 7474,
        path: '/db/data/batch',
        method: 'POST',
        headers: {
            'content-type': 'application/json',
            'accept': 'application/json'
        }
    };

    var req = http.request(options, function(res) {
        res.on('data', function (chunk) {
            // console.log(chunk.toString());
            if (_.isFunction(callback)) callback(false);
        });
    });

    req.on('error', function (e) {
        // console.log(e.toString());
        if (_.isFunction(callback)) callback(true);
    });
    // console.log(prettyjson.render(object));
    req.write(JSON.stringify(object));
    req.end();
}

function createLineStream (file) {
    return readline.createInterface({
        input: fs.createReadStream(file),
        output: new stream,
        terminal: false
    });
}
