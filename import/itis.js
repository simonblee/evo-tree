/**
 * Import script from ITIS MySQL to custom table structure
 */

var mysql = require('mysql');

var dbITIS = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'itis'
});

var dbEvoTree = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'evo-tree'
});

var createdAt = dbEvoTree.escape(new Date());

// NOTE: MUST run itisMerge.sql on new ITIS database. Import must be done after evo-tree DB is created
insertHierarchy();

// Insert Ranks table data
function insertRanks () {
    var query = dbITIS.query('SELECT * FROM ranks');
    query
        .on('error', onQueryError)
        .on('result', function (row) {
            dbITIS.pause();
            var query = "INSERT INTO Rank (id, name, created_at) VALUES ("+row.id+", '"+row.name+"', "+createdAt+")";
            console.log(query);
            dbEvoTree.query(query, function (err, results) {
                if (err) onQueryError(err);
                dbITIS.resume();
            });
        })
        .on('end', function() {
            console.log('All ranks inserted');
            insertTaxonAuthors();
        });
}

function insertTaxonAuthors () {
    var query = dbITIS.query('SELECT * FROM taxon_authors_lkp');
    query
        .on('error', onQueryError)
        .on('result', function (row) {
            dbITIS.pause();
            var query = "INSERT INTO TaxonAuthor (id, name, created_at) VALUES (" + row.taxon_author_id + "," + dbEvoTree.escape(row.short_author) + "," + createdAt + ")";
            console.log(query);
            dbEvoTree.query(query, function (err, results) {
                if (err) onQueryError(err);
                dbITIS.resume();
            });
        })
        .on('end', function() {
            console.log('All taxons inserted');
            insertTaxons();
        });
}

// TODO: Handle Taxon Authors too
function insertTaxons () {
    var query = dbITIS.query('SELECT *, ranks.id as new_rank_id FROM taxonomic_units tu ' +
                             'LEFT JOIN taxon_unit_types tut ON tu.rank_id = tut.rank_id AND tu.kingdom_id = tut.kingdom_id ' +
                             'LEFT JOIN ranks ON tut.rank_name = ranks.name ' +
                             'LEFT JOIN taxon_authors_lkp ON tu.taxon_author_id = taxon_authors_lkp.taxon_author_id');
    query
        .on('error', onQueryError)
        .on('result', function (row) {
            dbITIS.pause();
            var query,
                values = '(' + row.tsn + ',' +
                dbEvoTree.escape(row.complete_name) + "," +
                (row.name_usage ? "'" + row.name_usage + "'," : 'NULL,') +
                (row.unaccept_reason ? "'" + row.unaccept_reason + "'," : 'NULL,') +
                (row.credibility_rtng ? "'" + row.credibility_rtng + "'," : 'NULL,') +
                (row.completeness_rtng ? "'" + row.completeness_rtng + "'," : 'NULL,') +
                (row.currency_rating ? "'" + row.currency_rating + "'," : 'NULL,') +
                row.new_rank_id + ', ' +
                (row.taxon_author_id ? row.taxon_author_id + "," : 'NULL,') +
                createdAt + ')';

            query = "INSERT INTO Taxon (id, name, nameUsage, unacceptReason, credibilityRating, completenessRating, currencyRating, rank_id, taxon_author_id, created_at) VALUES " + values
            console.log(query);
            dbEvoTree.query(query, function (err, results) {
                if (err) onQueryError(err);
                dbITIS.resume();
            });
        })
        .on('end', function() {
            console.log('All taxons inserted');
            insertDataSources();
        });
}

function insertDataSources () {
    var query = dbITIS.query('SELECT * FROM other_sources');
    query
        .on('error', onQueryError)
        .on('result', function (row) {
            dbITIS.pause();
            var values = '(' +
                row.source_id + ',' +
                (row.source_type ? "'" + row.source_type + "'," : 'NULL,') +
                (row.source ? dbEvoTree.escape(row.source) + "," : 'NULL,') +
                (row.version ? "'" + row.version + "'," : 'NULL,') +
                dbEvoTree.escape(row.acquisition_date) +', '+
                (row.source_comment ? dbEvoTree.escape(row.source_comment) + "," : 'NULL,') +
                createdAt + ')';

            var query = "INSERT INTO DataSource (id, type, source, version, acquiredAt, comment, created_at) VALUES " + values

            console.log(query);
            dbEvoTree.query(query, function (err, results) {
                if (err) onQueryError(err);
                dbITIS.resume();
            });
        })
        .on('end', function() {
            console.log('All data sources inserted');
            insertExperts();
        });
}

function insertExperts () {
    var query = dbITIS.query('SELECT * FROM experts');
    query
        .on('error', onQueryError)
        .on('result', function (row) {
            dbITIS.pause();
            var values = '(' +
                row.expert_id + ',' +
                (row.expert ? "'" + row.expert + "'," : 'NULL,') +
                (row.exp_comment ? dbEvoTree.escape(row.exp_comment) + "," : 'NULL,') +
                createdAt + ')';

            var query = "INSERT INTO Expert (id, name, comment, created_at) VALUES " + values
            console.log(query);
            dbEvoTree.query(query, function (err, results) {
                if (err) onQueryError(err);
                dbITIS.resume();
            });
        })
        .on('end', function() {
            console.log('All experts inserted');
            insertGeographicDivisions();
        });
}

function insertGeographicDivisions () {
    var query = dbITIS.query('SELECT * FROM geographic_div');
    query
        .on('error', onQueryError)
        .on('result', function (row) {
            dbITIS.pause();
            var values = '(' +
                (row.geographic_value ? "'" + row.geographic_value + "'," : 'NULL,') +
                row.tsn + ',' +
                createdAt + ')';

            var query = "INSERT INTO GeographicDivision (name, taxon_id, created_at) VALUES " + values
            console.log(query);
            dbEvoTree.query(query, function (err, results) {
                if (err) onQueryError(err);
                dbITIS.resume();
            });
        })
        .on('end', function() {
            console.log('All experts inserted');
            insertJurisdictions();
        });
}

function insertJurisdictions () {
    var query = dbITIS.query('SELECT * FROM jurisdiction');
    query
        .on('error', onQueryError)
        .on('result', function (row) {
            dbITIS.pause();
            var values = '(' +
                (row.jurisdiction_value ? "'" + row.jurisdiction_value + "'," : 'NULL,') +
                (row.origin ? "'" + row.origin + "'," : 'NULL,') +
                row.tsn + ',' +
                createdAt + ')';

            var query = "INSERT INTO Jurisdiction (name, origin, taxon_id, created_at) VALUES " + values
            console.log(query);
            dbEvoTree.query(query, function (err, results) {
                if (err) onQueryError(err);
                dbITIS.resume();
            });
        })
        .on('end', function() {
            console.log('All experts inserted');
            insertPublications();
        });
}

function insertPublications () {
    var query = dbITIS.query('SELECT * FROM publications');
    query
        .on('error', onQueryError)
        .on('result', function (row) {
            dbITIS.pause();
            var values = '(' +
                row.publication_id + ',' +
                (row.reference_author ? dbEvoTree.escape(row.reference_author) + "," : 'NULL,') +
                (row.title ? dbEvoTree.escape(row.title) + "," : 'NULL,') +
                (row.publication_name ? dbEvoTree.escape(row.publication_name) + "," : 'NULL,') +
                (row.listed_pub_date ? dbEvoTree.escape(row.listed_pub_date) + "," : 'NULL,') +
                (row.actual_pub_date ? dbEvoTree.escape(row.actual_pub_date) + "," : 'NULL,') +
                (row.publisher ? dbEvoTree.escape(row.publisher) + "," : 'NULL,') +
                (row.pub_place ? dbEvoTree.escape(row.pub_place) + "," : 'NULL,') +
                (row.isbn ? "'" + row.isbn + "'," : 'NULL,') +
                (row.issn ? "'" + row.issn + "'," : 'NULL,') +
                (row.pages ? "'" + row.pages + "'," : 'NULL,') +
                (row.pub_comment ? dbEvoTree.escape(row.pub_comment) + "," : 'NULL,') +
                createdAt + ')';

            var query = "INSERT INTO Publication (id, author, title, publicationName, listedPublicationDate, actualPublicationDate, publisher, publicationPlace, isbn, issn, pages, comment, created_at) VALUES " + values
            console.log(query);
            dbEvoTree.query(query, function (err, results) {
                if (err) onQueryError(err);
                dbITIS.resume();
            });
        })
        .on('end', function() {
            console.log('All experts inserted');
            insertSynonymTaxons();
        });
}

function insertSynonymTaxons () {
    var query = dbITIS.query('SELECT * FROM synonym_links');
    query
        .on('error', onQueryError)
        .on('result', function (row) {
            dbITIS.pause();
            var values = '(' +
                row.tsn + ',' +
                row.tsn_accepted + ',' +
                createdAt + ')';

            var query = "INSERT INTO SynonymTaxon (synonym_id, taxon_id, created_at) VALUES " + values
            console.log(query);
            dbEvoTree.query(query, function (err, results) {
                if (err) onQueryError(err);
                dbITIS.resume();
            });
        })
        .on('end', function() {
            console.log('All experts inserted');
            insertVernaculars();
        });
}

function insertVernaculars () {
    var query = dbITIS.query('SELECT * FROM vernaculars');
    query
        .on('error', onQueryError)
        .on('result', function (row) {
            dbITIS.pause();
            var values = '(' +
                row.vern_id + ',' +
                (row.vernacular_name ? dbEvoTree.escape(row.vernacular_name) + "," : 'NULL,') +
                (row.language ? "'" + row.language + "'," : 'NULL,') +
                (!row.approved_ind || row.approved_ind === 'N' ? 0 + ',' : 1 + ',') +
                (row.tsn ? row.tsn + ',' : 'NULL,') +
                createdAt + ')';

            var query = "INSERT INTO Vernacular (id, name, language, approved, taxon_id, created_at) VALUES " + values
            console.log(query);
            dbEvoTree.query(query, function (err, results) {
                if (err) onQueryError(err);
                dbITIS.resume();
            });
        })
        .on('end', function() {
            console.log('All experts inserted');
            insertHierarchy();
        });
}

// Create linking table data - currently doesn't exist in ITIS DB. Needs
// to be created from their data.
function insertHierarchy () {
    var query = dbITIS.query('SELECT * FROM hierarchy');
    query
        .on('error', onQueryError)
        .on('result', function (row) {
            dbITIS.pause();
            var depth, queryParts = [],
                query = "INSERT INTO Hierarchy (ancestor_id, descendant_id, depth, created_at) VALUES ",
                hierarchy = row.hierarchy_string.split('-'),
                descendant = hierarchy[hierarchy.length - 1];

            // ITIS db does include a root node in the hierarchy string. itisMerge.sql will
            // manually insert a root node into the taxon table. Just need to prepend each
            // hierarchy string with its ID which is 0. No other part of the hierarchy table
            // is used.
            hierarchy = ['0'].concat(hierarchy);

            if (hierarchy.length === 1) {
                ancestor = hierarchy[0];
                depth = 1;
                query += "(" + ancestor + "," + descendant + "," + depth + "," + createdAt + ")";
            } else {
                for (var i = 0, len = hierarchy.length; i < len; i++) {
                    depth = i + 1;
                    ancestor = hierarchy[hierarchy.length - depth];
                    queryParts.push("(" + ancestor + "," + descendant + "," + depth + "," + createdAt + ")");
                }

                query += queryParts.join(',');
            }

            console.log(query);
            dbEvoTree.query(query, function (err, results) {
                if (err) onQueryError(err);
                dbITIS.resume();
            });
        })
        .on('end', function() {
            console.log('All hierarchy inserted');
            insertTaxonReferenceLinks();
        });
}

// For publications, experts and data sources
function insertTaxonReferenceLinks () {
    var query = dbITIS.query('SELECT * FROM reference_links');
    query
        .on('error', onQueryError)
        .on('result', function (row) {
            var query;

            dbITIS.pause();

            if (row.doc_id_prefix === 'SRC') {
                query = "INSERT INTO DataSourceTaxon (taxon_id, data_source_id, created_at) VALUES (" + row.tsn + "," + row.documentation_id + "," + createdAt + ")";
            } else if (row.doc_id_prefix === 'PUB') {
                query = "INSERT INTO PublicationTaxon (taxon_id, publication_id, created_at) VALUES (" + row.tsn + "," + row.documentation_id + "," + createdAt + ")";
            } else if (row.doc_id_prefix === 'EXP') {
                query = "INSERT INTO ExpertTaxon (taxon_id, expert_id, created_at) VALUES (" + row.tsn + "," + row.documentation_id + "," + createdAt + ")";
            }

            console.log(query);
            dbEvoTree.query(query, function (err, results) {
                if (err) onQueryError(err);
                dbITIS.resume();
            });
        })
        .on('end', function() {
            console.log('All experts inserted');
        });
}

function onQueryError (err) {
    console.log(err);
    process.exit(1);
}
