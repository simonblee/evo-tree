-- Truncate DB
-- SET foreign_key_checks = 0;
-- truncate rank;
-- truncate taxonauthor;
-- truncate taxon;

-- Create a new rank table and point all taxons to this new table
CREATE TABLE ranks (`id` INTEGER auto_increment , `name` VARCHAR(20), PRIMARY KEY (`id`)) ENGINE=MyISAM;
INSERT INTO ranks (name) VALUES
    ('Root'),
    ('Kingdom'),
    ('Subkingdom'),
    ('Phylum'),
    ('Subphylum'),
    ('Superclass'),
    ('Class'),
    ('Subclass'),
    ('Infraclass'),
    ('Superorder'),
    ('Order'),
    ('Suborder'),
    ('Infraorder'),
    ('Superfamily'),
    ('Family'),
    ('Subfamily'),
    ('Tribe'),
    ('Subtribe'),
    ('Genus'),
    ('Subgenus'),
    ('Species'),
    ('Subspecies'),
    ('Variety'),
    ('Infrakingdom'),
    ('Division'),
    ('Subdivision'),
    ('Infradivision'),
    ('Section'),
    ('Subsection'),
    ('Subvariety'),
    ('Form'),
    ('Subform'),
    ('Superphylum'),
    ('Infraphylum'),
    ('Race'),
    ('Stirp'),
    ('Morph'),
    ('Aberration'),
    ('Unspecified');

-- Need to insert a root node into the taxon db
INSERT INTO `taxon_unit_types` (`kingdom_id`, `rank_id`, `rank_name`, `dir_parent_rank_id`, `req_parent_rank_id`, `update_date`)
VALUES
    (1, 1, 'Root', 10, 10, '1996-07-11');

INSERT INTO `taxonomic_units` (`tsn`, `unit_ind1`, `unit_name1`, `unit_ind2`, `unit_name2`, `unit_ind3`, `unit_name3`, `unit_ind4`, `unit_name4`, `unnamed_taxon_ind`, `name_usage`, `unaccept_reason`, `credibility_rtng`, `completeness_rtng`, `currency_rating`, `phylo_sort_seq`, `initial_time_stamp`, `parent_tsn`, `taxon_author_id`, `hybrid_author_id`, `kingdom_id`, `rank_id`, `update_date`, `uncertain_prnt_ind`, `n_usage`, `complete_name`)
VALUES
    (0, NULL, 'Root', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'valid', NULL, 'No review; untreated NODC data', NULL, NULL, NULL, '2014-07-02 22:35:12', NULL, NULL, NULL, 1, 1, '2014-07-02', NULL, NULL, NULL);
