var app = require('../app'),
    request = require('supertest');

/**
 * Logs the user in by fetching their cookie. Callback is passed
 * the cookie as a parameter.
 */
module.exports.login = function (username, password, callback) {
    request(app)
        .post('/login')
        .send({
            username: username,
            password: password
        })
        .end(function (err, res) {
            var cookie = res.headers['set-cookie'].pop().split(';')[0];
            callback(cookie);
        });
}
