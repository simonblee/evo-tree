var Taxon,
    mocha = require('mocha'),
    chai = require('chai').should();

describe('Taxon model:', function () {

    before(function (done) {
        var db = require('../../config/config').get('sequelize');
        Taxon = db.import('../../models/taxon');
        done();
    });

    it('findChildren returns children of taxon', function (done) {
        Taxon.findChildren('Animalia', 'phylum').success(function (res) {
            res.should.have.length(3);
            res[1].name.should.equal('Porifera');
            done();
        }).error(done);
    });

    it('findDirectChildren returns direct (1 level down) children of taxon', function (done) {
        Taxon.findDirectChildren('Porifera').success(function (res) {
            res.should.have.length(3);
            res[1].rank_id.should.equal(3); // rank_id = 3 -> rank = class
            done();
        }).error(done);
    });

    it('findByRank returns list of taxon by rank', function (done) {
        Taxon.findByRank('class').success(function (res) {
            res.should.have.length(3);
            res[1].name.should.equal('Demospongiae');
            done();
        }).error(done);
    });

});
