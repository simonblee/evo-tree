var app = require('../../app'),
    request = require('supertest'),
    should = require('chai').should();

describe('Authentication Routes:', function () {

    it('Can login with username and password', function (done) {
        request(app)
            .post('/login')
            .send({
                username: 'lordsnow',
                password: 'password'
            })
            .expect(200)
            .expect('set-cookie', /connect.sid/, done);
    });

    // TODO: Login with email or username
    // it('Can login with email and password', function (done) {
    //     request(app)
    //         .post('/login')
    //         .send({
    //             username: 'lordsnow@thewall.com',
    //             password: 'password'
    //         })
    //         .expect(200)
    //         .end(done);
    // });

    it('Cannot login with bad password', function (done) {
        request(app)
            .post('/login')
            .send({
                username: 'lordsnow',
                password: 'badpassword'
            })
            .expect(401, done);
    });

    it('Cannot login with bad username', function (done) {
        request(app)
            .post('/login')
            .send({
                username: 'badusername',
                password: 'password'
            })
            .expect(401, done);
    });

});
