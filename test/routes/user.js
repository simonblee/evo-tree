var app = require('../../app'),
    request = require('supertest'),
    helper = require('../helper'),
    should = require('chai').should();

describe('User Routes:', function () {

    it('Responds with list of users', function (done) {
        request(app)
            .get('/users')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                res.body.length.should.equal(4);
                res.body[1].name.should.equal('Brandon Stark');
                res.body[1].url.should.equal('/users/2');
                done(err, res);
            });
    });

    it('Responds with single user', function (done) {
        request(app)
            .get('/users/1')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                res.body.name.should.equal('John Snow');
                done(err, res);
            });
    });

    it('Updates existing user', function (done) {
        var newName = 'robert baratheon';

        helper.login('lordsnow', 'password', function (cookie) {
            request(app)
                .put('/users/1')
                .set('Cookie', cookie)
                .send({ name: newName })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    res.body.name.should.equal(newName);
                    request(app)
                        .get('/users/1')
                        .end(function (err, res) {
                            res.body.name.should.equal(newName);
                            done(err, res);
                        });
                });
        });
    });

    it('Guest cannot update user', function (done) {
        request(app)
            .put('/users/1')
            .send({ name: 'some new name' })
            .expect(401, done);
    });

    it('User cannot update a different user', function (done) {
        // Login with user id 2 and attempt to update user id 1
        helper.login('bran', 'password', function (cookie) {
            request(app)
                .put('/users/1')
                .set('Cookie', cookie)
                .send({ name: 'some other name' })
                .expect(403, done);
        });
    });

    // TODO: Check all of user's stuff get's deleted, ie. lists n shit.
    it('User can delete their account', function (done) {
        helper.login('lordsnow', 'password', function (cookie) {
            request(app)
                .del('/users/1')
                .set('Cookie', cookie)
                .expect('Content-Type', /json/)
                .expect(204) // 204 = no content, ie. deleted
                .end(function (err, res) {
                    should.not.exist(res);
                    request(app)
                        .get('/users/1')
                        .expect(404, done);
                });
        });
    });

    it('Guest cannot delete user', function (done) {
        request(app)
            .del('/users/1')
            .expect(401, done);
    });

    it('User cannot delete a different user', function (done) {
        helper.login('bran', 'password', function (cookie) {
            request(app)
                .del('/users/1')
                .set('Cookie', cookie)
                .expect(403, done);
        });
    });

});
