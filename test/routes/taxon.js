var app = require('../../app'),
    request = require('supertest'),
    should = require('chai').should();

describe('Taxon Routes:', function () {

    // Taxon with id 5 -> Calcarea
    // Taxon with id 6 -> Demospongiae
    // Both above taxa have ancestors 'Animalia' and 'Porifera'
    it('responds with ancestors of two taxa as json', function (done) {
        request(app)
            .get('/taxon/ancestors/5/6')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                res.body[0].name.should.equal('Animalia');
                res.body[1].name.should.equal('Porifera');
                done(err, res);
            });
    });

    it('responds with ancestors of single taxon as json', function (done) {
        request(app)
            .get('/taxon/ancestors/2')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                res.body[0].name.should.equal('Animalia');
                done(err, res);
            });
    });

    it('responds with taxon list as json', function (done) {
        request(app)
            .get('/kingdom')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                res.body.should.have.length(1);
                done(err, res);
            });
    });

    it('responds with taxon list after search as json', function (done) {
        request(app)
            .get('/taxon/search/Anim')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                res.body.should.have.length(1);
                done(err, res);
            });
    });

    it('responds with a specific taxon as json', function (done) {
        request(app)
            .get('/taxon/Animalia') // sqlite is case sensitive, use uppercase name
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                res.body.name.should.equal('Animalia');
                done(err, res);
            });
    });

    it('responds with specific children of taxon as json', function (done) {
        request(app)
            .get('/taxon/Animalia/class')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                res.body.should.have.length(3);
                done(err, res);
            });
    });

    it('responds with direct children of taxon as json', function (done) {
        request(app)
            .get('/taxon/Animalia/children')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                res.body.should.have.length(3); // Animalia has 3 direct children: myxozoa, porifera, cnidaria
                done(err, res);
            });
    });

});
