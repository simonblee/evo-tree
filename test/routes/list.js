var app = require('../../app'),
    request = require('supertest'),
    helper = require('../helper'),
    should = require('chai').should();

describe('List Routes:', function () {

    it('responds with list of user\'s lists', function (done) {
        // First user has two lists
        request(app)
            .get('/users/1/lists')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                res.body.length.should.equal(2);
                res.body[1].name.should.equal('Extinct Fish');
                res.body[1].url.should.equal('/lists/2');

                // Second user has no lists
                request(app)
                    .get('/users/2/lists')
                    .expect('Content-Type', /json/)
                    .expect(200)
                    .end(function (err, res) {
                        res.body.length.should.equal(0);
                        done(err, res);
                    });
            });
    });

    it('responds with single list', function (done) {
        request(app)
            .get('/lists/1')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                res.body.name.should.equal('Awesome Mammals');
                res.body.user_id.should.equal(1);
                done(err, res);
            });
    });

    it('creates a new list', function (done) {
        helper.login('lordsnow', 'password', function (cookie) {
            var listName = 'My Awesome List!!';

            request(app)
                .post('/users/1/lists')
                .set('Cookie', cookie)
                .send({name: listName})
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    res.body.name.should.equal(listName);
                    request(app)
                        .get('/lists/'+res.body.id)
                        .end(function (err, res) {
                            res.body.name.should.equal(listName);
                            done(err, res);
                        });
                });
        });
    });

    it('updates existing list', function (done) {
        var newName = 'Ugly Mammals';

        request(app)
            .put('/lists/1')
            .send({ name: newName })
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                res.body.name.should.equal(newName);
                request(app)
                    .get('/lists/1')
                    .end(function (err, res) {
                        res.body.name.should.equal(newName);
                        done(err, res);
                    });
            });
    });

    it('deletes existing list', function (done) {
        request(app)
            .del('/lists/1')
            .expect('Content-Type', /json/)
            .expect(204) // 204 = no content, ie. deleted
            .end(function (err, res) {
                should.not.exist(res);
                request(app)
                    .get('/lists/1')
                    .expect(404, done)
            });
    });

});
