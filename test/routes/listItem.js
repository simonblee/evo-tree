var app = require('../../app'),
    request = require('supertest'),
    should = require('chai').should();

describe('ListItem Routes:', function () {

    it('responds with list of list\'s items', function (done) {
        request(app)
            .get('/lists/2/items')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                res.body.length.should.equal(3);
                res.body[0].notes.should.equal('An expert on fish.');
                res.body[0].url.should.equal('/lists/2/items/1');
                res.body[0].user.name.should.equal('Brandon Stark');
                res.body[1].taxon.name.should.equal('Calcarea');

                // Second list has no items
                request(app)
                    .get('/lists/1/items')
                    .expect('Content-Type', /json/)
                    .expect(200)
                    .end(function (err, res) {
                        res.body.length.should.equal(0);
                        done(err, res);
                    });
            });
    });

    it('responds with single list item', function (done) {
        request(app)
            .get('/lists/2/items/2')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                res.body.notes.should.equal('I Love this animal!');
                res.body.list_id.should.equal(2);
                done(err, res);
            });
    });

    it('creates a new list item', function (done) {
        var itemNotes = 'Animalia, dawg.',
            taxonId = 1;

        request(app)
            .post('/lists/1/items')
            .send({
                notes: itemNotes,
                list_id: 1, // TODO: Should be set from the route's list_id parameter
                taxon_id: taxonId
            })
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                res.body.notes.should.equal(itemNotes);
                res.body.taxon.name.should.equal('Animalia');
                request(app)
                    .get('/lists/1/items/'+res.body.id)
                    .end(function (err, res) {
                        res.body.notes.should.equal(itemNotes);
                        res.body.taxon_id.should.equal(taxonId);
                        done(err, res);
                    });
            });
    });

    it('updates existing list item', function (done) {
        var newNotes = 'Some notes';

        request(app)
            .put('/lists/2/items/1')
            .send({ notes: newNotes })
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                res.body.notes.should.equal(newNotes);
                request(app)
                    .get('/lists/2/items/1')
                    .end(function (err, res) {
                        res.body.notes.should.equal(newNotes);
                        done(err, res);
                    });
            });
    });

    it('deletes existing list item', function (done) {
        request(app)
            .del('/lists/2/items/1')
            .expect('Content-Type', /json/)
            .expect(204) // 204 = no content, ie. deleted
            .end(function (err, res) {
                should.not.exist(res);
                request(app)
                    .get('/lists/2/items/1')
                    .expect(404, done)
            });
    });

});
