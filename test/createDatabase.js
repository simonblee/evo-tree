// Module create a database with some basic data to test with.
var allUsers, allLists, allTaxons, allRanks,
    _ = require('lodash'),
    fs = require('fs'),
    app = require('../app'),
    config = require('../config/config'),
    db = config.get('sequelize'),
    acl = config.get('acl'),
    dbSync = require('../models/sync'),
    bcrypt = require('bcryptjs');

// Outside of a describe function so runs before any test is executed.
before(function (done) {
    dbSync(
        db,
        function () {
            // Kick off test database creation
            createUsers(db, function () {
                app.initialise(done);
            });
        },
        function (error) {
            console.log(error);
        }
    );
});

after(function (done) {
    fs.unlink(config.getParam('database').options.storage, done);
});

function createUsers (db, done) {
    var passwordHash = bcrypt.hashSync('password', 10),
        User = db.import('../models/user');

    User.bulkCreate([
        {name: 'John Snow', username: 'lordsnow', email: 'lordsnow@thewall.com', password: passwordHash},
        {name: 'Brandon Stark', username: 'bran', email: 'bran@winterfell.com', password: passwordHash},
        {name: 'Ramsay Snow', username: 'bastardofbolton', email: 'ramsay@flayedman.com', password: passwordHash},
        {name: 'Theon Greyjoy', username: 'reek', email: 'theon@praiseramsay.com', password: passwordHash}
    ]).success(function () {
        User.findAll().success(function (users) {
            allUsers = users;
            users.forEach(function (user) {
                createUserAcls(user);
            });
            createLists(db, done);
        });
    });
}

function createLists (db, done) {
    var List = db.import('../models/list');

    List.bulkCreate([
        {name: 'Awesome Mammals', user_id: allUsers[0].id, description: 'A list of mammals that I think are awesome.'},
        {name: 'Extinct Fish', user_id: allUsers[0].id, description: 'Fish that have gone extinct in the last 200 000 years.'}
    ]).success(function () {
        List.findAll().success(function (lists) {
            allLists = lists;
            createRanks(db, done);
        });
    });
}

function createRanks (db, done) {
    var Rank = db.import('../models/rank');

    Rank.bulkCreate([
        {name: 'kingdom', level: 1},
        {name: 'phylum', level: 2},
        {name: 'class', level: 3}
    ]).success(function () {
        Rank.findAll().success(function (ranks) {
            allRanks = ranks;
            createTaxons(db, done);
        });
    });
}

// Create 7 taxons
function createTaxons (db, done) {
    var Taxon = db.import('../models/taxon');

    Taxon.bulkCreate([
        {name: 'Animalia', nameUsage: 'valid', rank_id: _.findWhere(allRanks, {name: 'kingdom'}).dataValues.id},
        {name: 'Myxozoa', nameUsage: 'valid', rank_id: _.findWhere(allRanks, {name: 'phylum'}).dataValues.id},
        {name: 'Porifera', nameUsage: 'valid', rank_id: _.findWhere(allRanks, {name: 'phylum'}).dataValues.id},
        {name: 'Cnidaria', nameUsage: 'valid', rank_id: _.findWhere(allRanks, {name: 'phylum'}).dataValues.id},
        {name: 'Calcarea', nameUsage: 'valid', rank_id: _.findWhere(allRanks, {name: 'class'}).dataValues.id},
        {name: 'Demospongiae', nameUsage: 'valid', rank_id: _.findWhere(allRanks, {name: 'class'}).dataValues.id},
        {name: 'Hexactinellida', nameUsage: 'valid', rank_id: _.findWhere(allRanks, {name: 'class'}).dataValues.id}
    ]).success(function () {
        Taxon.findAll().success(function (taxons) {
            allTaxons = taxons;
            createHierarchy(db, done);
        });
    });
}

// Create hierarchy for taxons
function createHierarchy (db, done) {
    var Hierarchy = db.import('../models/hierarchy');

    var animalia = _.findWhere(allTaxons, {name: 'Animalia'}).dataValues.id,
        myxozoa = _.findWhere(allTaxons, {name: 'Myxozoa'}).dataValues.id,
        porifera = _.findWhere(allTaxons, {name: 'Porifera'}).dataValues.id,
        cnidaria = _.findWhere(allTaxons, {name: 'Cnidaria'}).dataValues.id,
        calcarea = _.findWhere(allTaxons, {name: 'Calcarea'}).dataValues.id,
        demospongiae = _.findWhere(allTaxons, {name: 'Demospongiae'}).dataValues.id,
        hexactinellida = _.findWhere(allTaxons, {name: 'Hexactinellida'}).dataValues.id;

    Hierarchy.bulkCreate([
        {ancestor_id: animalia, descendant_id: animalia, depth: 1},
        {ancestor_id: animalia, descendant_id: myxozoa, depth: 2},
        {ancestor_id: animalia, descendant_id: porifera, depth: 2},
        {ancestor_id: animalia, descendant_id: cnidaria, depth: 2},
        {ancestor_id: animalia, descendant_id: calcarea, depth: 3},
        {ancestor_id: animalia, descendant_id: demospongiae, depth: 3},
        {ancestor_id: animalia, descendant_id: hexactinellida, depth: 3},
        {ancestor_id: porifera, descendant_id: porifera, depth: 1},
        {ancestor_id: porifera, descendant_id: calcarea, depth: 2},
        {ancestor_id: porifera, descendant_id: demospongiae, depth: 2},
        {ancestor_id: porifera, descendant_id: hexactinellida, depth: 2},
    ]).success(function () {
        createListItems(db, done);
    });
}

// Add some taxons to each list
function createListItems (db, done) {
    var ListItem = db.import('../models/listItem');

    ListItem.bulkCreate([
        {notes: 'An expert on fish.', list_id: allLists[1].id, user_id: allUsers[1].id, taxon_id: null, relationship_id: null},
        {notes: 'I Love this animal!', list_id: allLists[1].id, taxon_id: _.findWhere(allTaxons, {name: 'Calcarea'}).dataValues.id, relationship_id: null, user_id: null},
        {notes: 'For my project.', list_id: allLists[1].id, taxon_id: _.findWhere(allTaxons, {name: 'Demospongiae'}).dataValues.id, relationship_id: null, user_id: null}
    ]).success(function () {
        if (done) done();
    });
}

function createUserAcls (user) {
    var role = 'user_' + user.id,
        userUrl = '/users/' + user.id,
        listsUrl = userUrl + '/lists',
        writeRoles = [role, 'admin'],
        readRoles = ['guest', 'user'];

    acl.addUserRoles(user.id, ['user', role]);
    acl.allow(writeRoles, userUrl, ['put', 'delete']);
    acl.allow(readRoles, userUrl, 'get');
    acl.allow(readRoles, listsUrl, 'get');
    acl.allow(writeRoles, listsUrl, 'post');
}
