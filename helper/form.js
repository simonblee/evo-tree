// Adds some helper functions for handling submitted forms

/**
 * Checks that the request only supplies the whitelisted
 * parameters. Returns true if valid or an array of parameters
 * not in the whitelist if invalid.
 */
module.exports.validateParameters = function (req, whitelist) {
    for (var key in req.body) {
        if (whitelist.indexOf(key) === -1) return false;
    }

    return true;
}
