var config = require('../config/config'),
    acl = config.get('acl');

var STATUS_UNAUTHORIZED = 401,
    STATUS_FORBIDDEN = 403;

/**
 * Authorisation middleware. If a user is required in session, pass true
 * for the 'requireUser' parameter. Returns middleware function to run for
 * a request.
 */
module.exports.authorise = function (requireUser) {
    return function (req, res, next) {
        var user = req.user ? req.user.id.toString() : 'guest';

        if (requireUser && user === 'guest') {
            res.status(STATUS_UNAUTHORIZED).end();
        }

        acl.isAllowed(user, req.url, req.method.toLowerCase(), function (err, allowed) {
            if (allowed) next();
            else {
                res.status(STATUS_FORBIDDEN).end();
            }
        });
    }
}
