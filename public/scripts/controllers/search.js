define([
    'underscore',
    'marionette',
    'collections/search',
    'views/search'
],

function (_, Marionette, Collection, View) {

    return Marionette.Controller.extend({

        // Pass in controller url (function or string), view and item templates.
        initialize: function (options) {
            this.initializeCollection(options);
            this.initializeView(options);

            // Show the view
            this.region = new Marionette.Region({el: options.el});
            this.region.show(this.view);

            this.bindEvents();
        },

        onClose: function () {
            this.region.close();
        },

        // Listens for internal events and triggers external module events
        bindEvents: function () {
            this.listenTo(this.view, 'itemview:selected', function (view, model) {
                this.trigger('selected', model);
            }, this);
            this.listenTo(this.view, 'showitems', function () {
                this.trigger('showitems');
            }, this);
            this.listenTo(this.view, 'hideitems', function () {
                this.trigger('hideitems');
            }, this);
        },

        /**
         * Allows setting the model for each item and the url to search
         */
        initializeCollection: function (options) {
            var allowedOptions = ['model', 'baseUrl'];
            this.collection = new Collection([], _.pick(options, allowedOptions));
        },

        initializeView: function (options) {
            var allowedOptions = ['template', 'emptyTemplate', 'itemTemplate', 'modelSelectValue'];
            options = _.pick(options, allowedOptions);
            options.collection = this.collection;
            this.view = new View(options);
        }

    });
});
