define([
    'underscore',
    'marionette',
    'collections/tree',
    'views/tree'
],

function (_, Marionette, Collection, View) {

    return Marionette.Controller.extend({

        /**
         * Pass in controller url (function or string), view and item templates.
         */
        initialize: function (options) {
            this.app = options.app;
            this.initializeCollection(options);
            this.initializeView();

            // Show the view
            this.region = new Marionette.Region({el: options.el});
            this.region.show(this.view);

            this.collection.on('hierarchyBuilt', this.view.buildTree, this.view);
        },

        initializeCollection: function (options) {
            var allowedOptions = ['model'];
            this.collection = new Collection([], _.pick(options, allowedOptions));
        },

        initializeView: function () {
            this.view = new View({
                collection: this.collection,
                className: 'full'
            });
        },

        onClose: function () {
            this.region.close();
        },

        buildTree: function (searchModel) {
            var model = this.collection.get(searchModel.id);
            if (! model) {
                model = this.collection.add(searchModel);
            }
            this.collection.buildHierarchy(model);
        }

    });

});
