define([
    "underscore",
    "marionette",
    "controllers/search",
    "controllers/tree",
    "models/taxon"
],

function (_, Marionette, SearchModule, TreeModule, TaxonModel) {

    var App = new Marionette.Application();

    // An init function for your main application object
    App.addInitializer(function () {
        this.root = '/';
    });

    App.addRegions({
        tree: "#tree"
    });

    // Create all app modules
    App.addInitializer(function () {
        this.search = new SearchModule({
            el: '#search',
            baseUrl: 'taxon/search/[query]?limit=5', // TODO: App parameter
            template: 'search',
            itemTemplate: 'searchItem',
            modelSelectValue: 'name',
            model: TaxonModel
        });

        this.tree = new TreeModule({
            el: '#tree',
            app: this,
            model: TaxonModel
        });
    });

    // Create all app-wide events
    App.addInitializer(function () {
        var self = this;

        this.search.on('selected', function (model) {
            self.tree.buildTree.call(self.tree, model);
        });
    });

    // Return the instantiated app (there should only be one)
    return App;

});
