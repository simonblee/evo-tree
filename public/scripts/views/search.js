define([
    'underscore',
    'jquery',
    'marionette',
    './searchItem'
],

function (_, $, Marionette, SearchItemView) {

    var UP = 38, DOWN = 40, ESC = 27;

    return Marionette.CompositeView.extend({

        itemView: SearchItemView,
        itemViewContainer: '.items',
        className: 'search',
        queryTimeout: 0,
        activeChildIndex: -1,
        itemsVisible: false,

        // Template must have an input with class .search-input
        events: {
            'focusin .search-input': 'showResults',
            'blur .search-input': 'hideResults',
            'keyup .search-input': 'search',
            'keydown .search-input': function (e) {
                this.select(e);
                this.navigateList(e);
            }
        },

        initialize: function (options) {
            _.extend(this, options);
            this.itemViewOptions = {
                template: this.itemTemplate,
                className: 'item'
            };

            // Hide the results if a child is selected
            this.listenTo(this, 'itemview:selected', function (view, model) {
                this.input.blur();
                this.hideResults(null, true);
                this.setValue(model.get(this.modelSelectValue));
                this.collection.reset();
            }, this);

            // Set this.activeChildIndex to the current child index,
            // unhighligt the currently highlighted view
            this.listenTo(this, 'itemview:highlight', function () {
                if (this.activeChildIndex > -1) this.children.findByIndex(this.activeChildIndex).unHighlight();
                this.activeChildIndex = this.items.find('.item').index(this.items.find('.active'));
            }, this);

            $(document).on('click', _.bind(this.hideResults, this));
        },

        // Cache important DOM elements
        onShow: function () {
            this.input = this.$('.search-input');
            this.items = this.$('.items');
        },

        setValue: function (value) {
            this.input.val(value);
        },

        showResults: function () {
            if (this.itemsVisible || ! this.children.length) return;
            this.items.show();
            this.itemsVisible = true;
            this.trigger('showitems');
        },

        hideResults: function (e, manual) {
            if (! this.mouseInView(e) || manual || ! this.children.length) {
                this.unHighlightChildren();
                this.itemsVisible = false;
                this.trigger('hideitems');
                this.items.hide();
            }
        },

        mouseInView: function (e) {
            var offset, x, y, left, right, top, bottom;

            if (!e || e.type !== 'click') {
                return true;
            }

            offset = this.$el.offset();
            x = e.clientX;
            y = e.clientY;
            left = offset.left;
            right = left + this.$el.width();
            top = offset.top;
            bottom = top + this.$el.height() + this.$itemViewContainer.height();

            if (left < x && x < right && top < y && y < bottom) {
                return true;
            }

            return false;
        },

        unHighlightChildren: function () {
            this.children.call('unHighlight');
            this.resetActiveChildIndex();
        },

        resetActiveChildIndex: function () {
            this.activeChildIndex = -1;
        },

        select: function (e) {
            var view = this.children.findByIndex(this.activeChildIndex);

            if (e.which === 13 && view) {
                view.select();
            }
        },

        // Use the up/down keys to navigate the list of items
        navigateList: function (e) {
            var nextIndex;

            if (!(e.which == UP || e.which == DOWN)) return;
            if (!this.itemsVisible) this.showResults();

            if (e.which == UP && this.activeChildIndex > -1) {
                nextIndex = this.activeChildIndex - 1;
            } else if (e.which == DOWN && this.activeChildIndex < this.children.length) {
                nextIndex = this.activeChildIndex + 1;
            }

            // Unselect the previous child, select the next child
            if (this.collection.length) {
                if (this.activeChildIndex >= 0 && nextIndex < this.children.length) this.getChildByIndex(this.activeChildIndex).unHighlight();
                if (nextIndex >= 0 && nextIndex < this.children.length) this.getChildByIndex(nextIndex).highlight();
                if (nextIndex >= -1 && nextIndex < this.children.length) this.activeChildIndex = nextIndex;
            }
        },

        getChildByIndex: function (index) {
            return this.children.findByIndex(index);
        },

        // Set up an optional empty view to show if no results are present
        getEmptyView: function() {
            if (this.emptyTemplate) {
                return Marionette.ItemView.extend({template: this.emptyTemplate});
            }

            return null;
        },

        search: function (e) {
            var self = this,
                value = $(e.currentTarget).val();

            // If trying to select an item in the list or just hide the list, don't search
            if (e.which == UP || e.which == DOWN) return;

            if (e.which == ESC) {
                this.hideResults(null, true);
                return;
            }

            if (! value) {
                this.collection.reset();
                this.resetActiveChildIndex();
                this.hideResults();
                return;
            }

            clearTimeout(this.queryTimeout);
            this.queryTimeout = setTimeout(function () {
                self.collection.query = value;
                self.collection.fetch({
                    reset: true,
                    success: function () {
                        self.showResults();
                    }
                });
                self.resetActiveChildIndex.call(self);
            }, 250);
        }

    });
});
