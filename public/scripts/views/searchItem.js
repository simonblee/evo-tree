define([
    'underscore',
    'marionette'
],

function (_, Marionette) {

    return Marionette.ItemView.extend({

        events: {
            'click': 'select',
            'mouseenter': 'highlight',
            'mouseleave': 'unHighlight'
        },

        // Clicking anywhere on the element will select it
        select: function (e) {
            this.trigger('selected', this.model);
        },

        highlight: function (manual) {
            this.$el.addClass('active');
            this.trigger('highlight');
        },

        unHighlight: function (manual) {
            this.$el.removeClass('active');
            this.trigger('unhighlight');
        }

    });
});
