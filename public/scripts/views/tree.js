define([
    'underscore',
    'jquery',
    'd3',
    'marionette',
    'utility',
    'text!icons.svg'
],

// http://bl.ocks.org/syntagmatic/2409451 - canvas and svg
// http://bl.ocks.org/mbostock/6238040 - zoom transitions
//
// TODO: Break up the svg nodes into smaller objects for
// easier referencing and handling.
function (_, $, d3, Marionette, Utility, icons) {

    return Marionette.View.extend({

        animate: false,
        radialGradient: 'node-radial-gradient',
        zoomMin: 0.05,
        zoomMax: 1,
        textLineHeight: 20, // px
        actionSize: 20,
        nodeRadius: 60,
        nodeWidth: 600,
        nodeHeight: 800,
        icons: {},

        events: {
            'click .action.children': 'showChildren',
            'click .action.parent': 'showParent',
            'click .action.relationships': 'showRelationships',
            'click .action.view': 'showNode',
            'click .action.add': 'addToList',
        },

        initialize: function () {
            $('body').prepend(icons);
        },

        buildTree: function (model, collection) {
            var nodeData = this.tree.nodes(collection.root);

            this.createLinks(nodeData);
            this.createNodes(nodeData);

            // Transition to the node
            this.moveToNode(model);
        },

        moveToNode: function (model) {
            var x = -1 * model.x * this.zoomListener.scale() + this.$('svg').width()/2,
                y = -1 * model.y * this.zoomListener.scale() + this.$('svg').height()/2;

            this.animate = true;
            this.zoomListener.translate([x, y]).event(this.d3Svg); // triggers zoom event
            this.animate = false;
        },

        // TODO: Do this in a the Node object's 'render' function. Node object
        // has not yet been created...but will be.
        createNodes: function (nodeData) {
            var newNodes, offset, gradients, group,
                self = this,
                nodes = this.group.selectAll('.node')
                    .data(nodeData, function (model) {
                        return model.id; // joins models to the node by id
                    });

            // Create a node container and add in the node data
            newNodes = nodes.enter().append('g')
                .attr('class', 'node');

            // Create each action element
            // view - show in depth information about taxa
            // relationships - show all relationships for taxa
            // parent - show parent
            // children - show children
            // add - add to a list, show a menu
            ['view', 'relationships', 'parent', 'children', 'add'].forEach(function (actionClass) {
                group = newNodes.append('g')
                    .attr('class', actionClass + ' action');

                group.append('svg:use')
                    .attr('xlink:href', '#icon-' + actionClass);

                // Allows clicking of entire group rather than just paths of icons
                group.append('rect')
                    .attr('class', 'action-filler')
                    .attr('width', '30')
                    .attr('height', '30');
            });

            // Image clip path
            newNodes.append('clipPath')
                .attr('id', function (model) {
                    return 'imageClip' + model.id;
                })
                .append('circle')
                    .attr('cx', '0')
                    .attr('cy', '0')
                    .attr('r', self.nodeRadius);

            newNodes.append('svg:image')
                .attr('width', self.nodeRadius * 2)
                .attr('height', self.nodeRadius * 2)
                .attr('x', -1 * self.nodeRadius)
                .attr('y', -1 * self.nodeRadius)
                .attr('clip-path', function (model) {
                    return 'url(#imageClip' + model.id + ')';
                })
                .attr('xlink:href', '/images/lion-240.jpeg'); // sample for testing

            newNodes.append('text')
                .text(function (model) {
                    return model.get('name');
                })
                .attr('y', this.textLineHeight/2)
                .attr('x', this.nodeRadius + 10)
                .call(Utility.wrap, 3*this.nodeRadius, this.textLineHeight);

            // Position all nodes
            nodes.attr('transform', function (model) {
                return 'translate(' + model.x + ',' + model.y + ')';
            });

            return newNodes;
        },

        createLinks: function (nodeData) {
            var links, newLinks,
                self = this,
                linkData = this.tree.links(nodeData);

            links = this.group.selectAll('.link')
                .data(linkData);

            newLinks = links.enter().insert('path', '.link')
                .attr('class', 'link');

            links.attr('d', d3.svg.diagonal().source(function (d) {
                return {
                    x: d.source.x,
                    y: d.source.y + self.nodeRadius + 4 * self.textLineHeight
                };
            }).target(function (d) {
                return {
                    x: d.target.x,
                    y: d.target.y - self.nodeRadius - 3 * self.textLineHeight
                };
            }));
        },

        /**
         * Zoom and pan the tree. If event is a double click, smoothly
         * transition to the new zoom and position.
         */
        zoom: function () {
            var isDblclick = d3.event.sourceEvent && d3.event.sourceEvent.type === 'dblclick',
                selection = (this.animate || isDblclick) ? this.group.transition().duration(250) : this.group;

            selection.attr('transform', 'translate(' + d3.event.translate + ')scale(' + d3.event.scale + ')');
        },

        render: function () {
            // Zooming and panning
            this.zoomListener = d3.behavior.zoom()
                .scaleExtent([this.zoomMin, this.zoomMax])
                .on('zoom', _.bind(this.zoom, this));

            // Create the core elements (TODO: Add these to a dust template?)
            this.d3Svg = d3.select(this.el).append('svg')
                .call(this.zoomListener);

            this.group = this.d3Svg.append('g')
                .attr('class', 'tree-wrapper');
        },

        onShow: function () {
            this.tree = d3.layout.tree()
                .nodeSize([this.nodeWidth, this.nodeHeight])
                .sort(function (a, b) {
                    return b.get('name').toLowerCase() < a.get('name').toLowerCase() ? 1 : -1;
                });
        },

        showChildren: function (e) {
            var model = e.currentTarget.__data__;
            this.collection.buildHierarchy(model);
        },

        showParent: function (e) {

        },

        showRelationships: function (e) {

        },

        showNode: function (e) {

        },

        /**
         * Show all user lists for adding the model to
         */
        addToList: function (e) {

        }

    });
});
