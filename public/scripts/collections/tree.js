define([
    'backbone',
    'models/taxon',
],

function (Backbone, TaxonModel) {

    return Backbone.Collection.extend({

        root: null,

        initialize: function (models, options) {
            this.on('add', this.connectModel, this);
        },

        buildHierarchy: function (model) {
            this.fetchModelChildren(model);
        },

        /**
         * Fetch IMMEDIATE children for the model
         */
        fetchModelChildren: function (model) {
            var self = this;

            model.get('children').on('add', function (child) {
                self.add(child);
            }).fetch({
                complete: function () {
                    self.fetchModelAncestors(model);
                }
            });
        },

        /**
         * Fetch all ancestors up to the root for the model
         */
        fetchModelAncestors: function (model) {
            var self = this,
                ancestorCollection = new Backbone.Collection([], {model: TaxonModel});

            ancestorCollection.url = appConfig.ancestorUrl.replace(':firstTaxon', model.id);
            ancestorCollection.on('add', function (ancestor) {
                self.add(ancestor);
            }).fetch({
                complete: function () {
                    self.connectAncestors(model, ancestorCollection);
                    self.trigger('hierarchyBuilt', model, self);
                }
            });
        },

        /**
         * Connect the model to the rest of the tree. Set the parent and
         * add to children if they are found in the collection
         */
        connectAncestors: function (model, ancestors) {
            var depth, parent, thisAncestor,
                maxDepth = 0,
                self = this;

            // Get the model in this collection matching 'ancestor' as the
            // new ancestor model may not have been added to this collection
            // because it may have already been there.
            ancestors.each(function (ancestor) {
                depth = ancestor.get('depth');
                thisAncestor = self.get(ancestor.id);
                parent = ancestors.findWhere({depth: depth + 1});

                if (depth > maxDepth) maxDepth = depth;
                if (depth === 1) return;
                else if (depth === 2) model.setParent(thisAncestor);
                if (parent) thisAncestor.setParent(self.get(parent.id));
            });

            if (ancestors.length) {
                this.root = this.get(ancestors.findWhere({depth: maxDepth}).id);
            } else if (! this.root) {
                this.root = model;
            }
        }

    });

});
