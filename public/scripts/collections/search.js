define([
    'backbone'
],

function (Backbone) {

    return Backbone.Collection.extend({

        query: '',

        initialize: function (models, options) {
            this.baseUrl = options.baseUrl;
        },

        url: function () {
            if (_.isFunction(this.baseUrl)) {
                return this.baseUrl(this.query);
            } else {
                return this.baseUrl.replace('[query]', this.query);
            }
        },

        fetch: function (options) {
            if (! this.query) this.reset();
            else {
                return Backbone.Collection.prototype.fetch.call(this, options);
            }
        }

    });

});
