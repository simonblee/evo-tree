define([
    'underscore',
    'jquery',
    'd3'
],

function (_, $, d3) {

    return {

        /**
         * Based on text wrapping from http://bl.ocks.org/mbostock/7555321
         *
         * Breaks a text node into a multiline text element with tspan
         * elements for each line.
         */
        wrap: function (textElements, width, lineHeight, lineHeightUnit) {
            textElements.each(function () {
                var word,
                    lineHeightUnit = lineHeightUnit || 'px',
                    text = d3.select(this),
                    words = text.text().split(/\s+/).reverse(), // split word on space
                    line = [],
                    x = text.attr("x"),
                    y = text.attr("y"),
                    dy = parseFloat(text.attr("dy") || 0),
                    tspan = text.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", dy + 'px');

                while (word = words.pop()) {
                    // Add the text to the tspan and the line cache for later check
                    line.push(word);
                    tspan.text(line.join(" "));

                    // If max width reached, pop off the word that goes over the line
                    // width, add the line to the tspan and save the overflowed text
                    // for the beginning of the next line.
                    if (tspan.node().getComputedTextLength() > width && line.length > 1) {
                        line.pop();
                        tspan.text(line.join(" "));
                        line = [word];

                        // Create another tspan for the next line
                        tspan = text.append("tspan")
                            .attr("x", x)
                            .attr("y", y)
                            .attr("dy", lineHeight + lineHeightUnit)
                            .text(word);
                    }
                }
            });
        }
    }
});
