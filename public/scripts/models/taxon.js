define([
    'backbone'
],

function (Backbone) {

    var TaxonModel = Backbone.Model.extend({

        // TODO: Every time a new model is created the children effectively get thrown out
        // the window
        initialize: function (attributes, options) {
            var children = new Backbone.Collection([], {model: TaxonModel});
            children.url = this.attributes.childrenUrl;
            children.on('add', this.onAddChild, this);
            this.set('children', children);
        },

        setParent: function (model) {
            this.set('parent', model);
            this.parent = model;
            model.get('children').add(this);
            model.children = model.get('children').models;
        },

        onAddChild: function (child, collection, options) {
            child.setParent(this);
            this.children = this.get('children').models;
        }

    });

    return TaxonModel;

});
