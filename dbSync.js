// Small app to sync the sequelize database
var _ = require('lodash'),
    fs = require('fs'),
    config = require('./config/config'),
    db = config.get('sequelize'),
    dbSync = require('./models/sync');

dbSync(
    db,
    function () {
        console.log('Done!');
    },
    function (error) {
        console.log(error);
    }
);
