var compiled,
    fs = require('fs'),
    express = require('express'),
    passport = require('passport'),
    methodOverride = require('method-override'),
    app = express(),
    router = new (require('reversable-router')),
    authenticationRoutes = require('./routes/authentication'),
    crudRoutes = require('./routes/crud'),
    taxonRoutes = require('./routes/taxon'),
    env = app.get('env'),
    dust = require('dustjs-linkedin');

//
// CONFIGURATION
// =============
//

router.extendExpress(app);
router.registerAppHelpers(app);

// Set router in the request to allow URL generation in controllers.
app.use(function (req, res, next) {
    req.router = router;
    next();
});

// app.use(express.bodyParser());
app.use(express.json());
app.use(express.urlencoded());
app.use(methodOverride('_method'))
app.use(express.cookieParser());
app.use(express.session({secret: 'Todo: pull the app secret from the app configuration file.' }));
app.use(passport.initialize());
app.use(passport.session());
app.use(app.router);
app.use(express.static(__dirname + '/public'));

// development only
if ('development' === env || 'test' === env) {
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
}

// production only
if ('production' === env) {
    app.use(express.errorHandler());
}

// Load templates from file system and compile
dust.onLoad = function (name, callback) {
    // Get src from filesystem
    fs.readFile(__dirname + name, function (err, data) {
        if (err) throw err;
        callback(err, data.toString());
    });
}

app.render = function (template, data, res) {
    dust.render(template, data, function (err, out) {
        if (err) throw err;
        res.send(out);
    });
}

app.initialise = function (callback) {
    // Root serve index.html TODO: Make a static file in nginx
    app.get('/', function (req, res) {
        app.render('/views/index.dust', {}, res);
    });

    // Setup all Routes (DB call so use callback)
    authenticationRoutes.setup(app);
    crudRoutes.setup(app);
    taxonRoutes.setup(app, callback);

    // Listen on port 3000
    app.listen(3000);
}

// Initialise or export the app
// TODO: Shouldn't have 'test' code here
if ('test' === env) {
    module.exports = app;
} else {
    app.initialise();
}
