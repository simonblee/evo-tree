// Generated on 2013-10-16 using generator-maryo 0.1.6
'use strict';
var mountFolder = function (connect, dir) {
    return connect.static(require('path').resolve(dir));
};

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {
    // load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    // configurable paths
    var yeomanConfig = {
        app: 'app',
        dist: 'dist'
    };

    grunt.initConfig({
        yeoman: yeomanConfig,
        watch: {
            less: {
                files: 'public/styles/*.less',
                tasks: "less:dev"
            },
            dust: {
                files: 'public/scripts/templates/*.dust',
                tasks: "dustjs"
            },
            svgstore: {
                files: 'public/images/icons/*.svg',
                tasks: "svgstore"
            }
        },
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        'build/*',
                        '!build/.git*'
                    ]
                }]
            },
            server: '.tmp'
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            all: [
                'Gruntfile.js',
                'public/scripts/{,*/}*.js',
                '!public/scripts/vendor/*',
                'test/spec/{,*/}*.js'
            ]
        },
        jasmine: {
            all: {
                src: 'public/scripts/**/*.js',
                options: {
                    specs: 'public/scripts/test/*.js',
                    template: require('grunt-template-jasmine-requirejs'),
                    templateOptions: {
                        // Change this to mock certain objects for tests
                        requireConfigFile: 'public/scripts/config.js',
                        requireConfig: {
                            baseUrl: 'public/scripts'
                        }
                    }
                }
            }
        },
        less: {
            dev: {
                options: {
                    // syncImport: true,
                    paths: ["public/styles/app.less"]
                },
                files: {
                    "public/styles/less.css": "public/styles/app.less"
                }
            },
            prod: {
                options: {
                    paths: ["assets/css"],
                    yuicompress: true
                },
                files: {
                    "path/to/result.css": "path/to/source.less"
                }
            }
        },
        svgstore: {
            options: {
                cleanup: true,
                prefix : 'icon-',
                svg: {
                    id: 'icon',
                }
            },
            default : {
                files: {
                    'public/scripts/icons.svg': ['public/images/icons/*.svg']
                }
            },
        },
        requirejs: {
            dist: {
                options: {
                    baseUrl: 'public/scripts',
                    optimize: 'none',
                    preserveLicenseComments: false,
                    useStrict: true,
                    wrap: true
                }
            }
        },
        rev: {
            dist: {
                files: {
                    src: [
                        'build/scripts/{,*/}*.js',
                        'build/styles/{,*/}*.css',
                        'build/images/{,*/}*.{png,jpg,jpeg,gif,webp}',
                        'build/styles/fonts/*'
                    ]
                }
            }
        },
        useminPrepare: {
            html: 'public/index.html',
            options: {
                dest: 'build'
            }
        },
        usemin: {
            html: ['build/{,*/}*.html'],
            css: ['build/styles/{,*/}*.css'],
            options: {
                dirs: ['build']
            }
        },
        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'public/images',
                    src: '{,*/}*.{png,jpg,jpeg}',
                    dest: 'build/images'
                }]
            }
        },
        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'public/images',
                    src: '{,*/}*.svg',
                    dest: 'build/images'
                }]
            }
        },
        cssmin: {
            dist: {
                files: {
                    'build/styles/main.css': [
                        '.tmp/styles/{,*/}*.css',
                        'public/styles/{,*/}*.css'
                    ]
                }
            }
        },
        htmlmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'app',
                    src: '*.html',
                    dest: 'build'
                }]
            }
        },
        dustjs: {
            compile: {
                files: {
                   "public/scripts/templates/compiled.js": ["public/scripts/templates/*.dust"]
                }
            }
        },

        // Put files not handled in other tasks here
        copy: {
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: 'app',
                    dest: 'build',
                    src: [
                        '*.{ico,txt}',
                        '.htaccess',
                        'images/{,*/}*.{webp,gif}',
                        'styles/fonts/*'
                    ]
                }]
            }
        },
        concurrent: {
            server: [

            ],
            test: [

            ],
            dist: [
                'imagemin',
                'svgmin',
                'htmlmin'
            ]
        }
    });

    grunt.registerTask('server', function (target) {
        if (target === 'dist') {
            return grunt.task.run(['build', 'open']);
        }

        grunt.task.run([
            'clean:server',
            'concurrent:server',
            'open',
            'watch'
        ]);
    });

    grunt.registerTask('test', [
        'clean:server',
        'concurrent:test',
        'jasmine'
    ]);

    grunt.registerTask('build', [
        'clean:dist',
        'useminPrepare',
        'concurrent:dist',
        'requirejs',
        'cssmin',
        'concat',
        'uglify',
        'copy',
        'rev',
        'usemin'
    ]);

    grunt.registerTask('default', [
        'jshint',
        'test',
        'build'
    ]);
};
