var commonTableOptions = require('./tableOptions'),
    _ = require('lodash');

module.exports = function(database, DataTypes) {
    var Taxon,
        DataSource = database.import('./dataSource'),
        Expert = database.import('./expert'),
        TaxonAuthor = database.import('./taxonAuthor'),
        Rank = database.import('./rank'),
        GeographicDivision = database.import('./geographicDivision'),
        Jurisdiction = database.import('./jurisdiction'),
        Publication = database.import('./publication'),
        Vernacular = database.import('./vernacular'),
        Hierarchy = database.import('./hierarchy');

    Taxon = database.define(
        'Taxon',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            name: DataTypes.STRING,
            nameUsage: DataTypes.STRING(12),
            unacceptReason: {
                type: DataTypes.STRING(50),
                allowNull: true
            },
            credibilityRating: DataTypes.STRING(40),
            completenessRating: {
                type: DataTypes.STRING(10),
                allowNull: true
            },
            currencyRating: {
                type: DataTypes.STRING(10),
                allowNull: true
            }
        },
        _.extend(commonTableOptions, {
            classMethods: {
                findByName: findByName,
                findAncestors: findAncestors,
                findParent: findParent,
                findChildren: findChildren,
                findDirectChildren: findDirectChildren,
                findByRank: findByRank
            },
            instanceMethods: {
                setUrls: function(url, parentUrl, childrenUrl) {
                    this.values.url = url;
                    this.values.parentUrl = parentUrl;
                    this.values.childrenUrl = childrenUrl;
                }
            }
        })
    );

    function findByName (name) {
        var params = {name: name},
            sql = 'SELECT t.id, t.name, r.id as rank_id, r.name as rank_name, r.level as rank_level, v.id as vernacular_id, v.name as vernacular_name ' +
                  'FROM Taxon t ' +
                  'LEFT JOIN Rank r on t.rank_id = r.id ' +
                  'LEFT JOIN Vernacular v on v.taxon_id = t.id ' +
                  'WHERE t.name LIKE :name ' +
                  'OR v.name LIKE :name';

        return database.query(sql, null, {raw: true}, params);
    }

    function findAncestors (firstTaxon, secondTaxon) {
        var params = {firstTaxon: firstTaxon},
            hasSecondTaxon = !_.isNaN(secondTaxon),
            sql = 'SELECT t.*, h1.descendant_id, h1.ancestor_id, h1.depth FROM Hierarchy h1 ' +
                  (hasSecondTaxon ? 'LEFT JOIN Hierarchy h2 ON h1.ancestor_id = h2.ancestor_id ' : '') +
                  'LEFT JOIN Taxon t on h1.ancestor_id = t.id ' +
                  'WHERE h1.descendant_id = :firstTaxon ' +
                  (hasSecondTaxon ? 'AND h2.descendant_id = :secondTaxon' : '');

        if (secondTaxon) params.secondTaxon = secondTaxon;

        return database.query(sql, null, {raw: true}, params);
    }

    // Class Funtions
    function findParent (taxon) {
        var sql = 'SELECT t.* FROM Taxon t LEFT JOIN Hierarchy h ON h.ancestor_id = t.id ';

        if (_.isNaN(parseInt(taxon, 10))) {
            sql += 'LEFT JOIN Taxon td ON td.id = h.descendant_id WHERE td.name = :descendant';
        } else {
            sql += 'WHERE h.descendant_id = :descendant';
        }

        sql += ' AND h.depth = 2'; // direct parent

        return database.query(sql, Taxon, {}, {descendant: taxon});
    }

    function findChildren (taxon, childType, limit, offset) {
        var params = {
                ancestor: taxon,
                childType: childType
            },
            sql = 'SELECT t.* FROM Taxon t LEFT JOIN Hierarchy h ON h.descendant_id = t.id LEFT JOIN Rank r ON t.rank_id = r.id ';

        sql = _findChildrenBase(sql, params) + getLimitOffsetString(limit, offset);

        return database.query(sql, Taxon, {}, params);
    }

    function findDirectChildren (taxon, limit, offset) {
        var params = {ancestor: taxon},
            sql = 'SELECT t.* FROM Taxon t LEFT JOIN Hierarchy h ON h.descendant_id = t.id ';

        sql = _findChildrenBase(sql, params) + ' AND h.depth = 2' + getLimitOffsetString(limit, offset);

        return database.query(sql, Taxon, {}, params);
    }

    function _findChildrenBase (sql, params, limit, offset) {
        if (_.isNaN(parseInt(params.ancestor, 10))) {
            sql += 'LEFT JOIN Taxon ta ON ta.id = h.ancestor_id WHERE ta.name = :ancestor';
        } else {
            sql += 'WHERE h.ancestor_id = :ancestor';
        }

        if (params.childType) {
            sql += ' AND r.name = :childType';
        }

        return sql;
    }

    function findByRank (parentType, limit, offset) {
        var params = {parentType: parentType},
            sql = 'SELECT t.* FROM Taxon t LEFT JOIN Rank r ON t.rank_id = r.id WHERE r.name = :parentType' + getLimitOffsetString(limit, offset);

        return database.query(sql, Taxon, {}, params);
    }

    function getLimitOffsetString (limit, offset) {
        var limit = limit <= 100 ? limit : 20;

        return ' LIMIT ' + (offset ? offset + ',' : '') + limit;
    }

    // Taxon associations
    Taxon.hasMany(Taxon, {as: 'Synonym'});
    // Rank.hasMany(Taxon, {foreignKeyConstraint: true});
    Taxon.belongsTo(Rank, {foreignKeyConstraint: true});

    // Datasource M2M Taxon
    Taxon.hasMany(DataSource, {foreignKeyConstraint: true});
    DataSource.hasMany(Taxon, {foreignKeyConstraint: true});

    // Publication M2M Taxon
    Taxon.hasMany(Publication, {foreignKeyConstraint: true});
    Publication.hasMany(Taxon, {foreignKeyConstraint: true});

    // Expert M2M Taxon
    Taxon.hasMany(Expert, {foreignKeyConstraint: true});
    Expert.hasMany(Taxon, {foreignKeyConstraint: true});

    Taxon.hasMany(Vernacular, {foreignKeyConstraint: true});
    TaxonAuthor.hasMany(Taxon, {foreignKeyConstraint: true});

    GeographicDivision.belongsTo(Taxon, {foreignKeyConstraint: true}); // TODO: just add to Taxon table
    Jurisdiction.belongsTo(Taxon, {foreignKeyConstraint: true}); // TODO: just add to Taxon table

    return Taxon;
}
