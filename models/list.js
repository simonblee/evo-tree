var commonTableOptions = require('./tableOptions'),
    _ = require('lodash');

module.exports = function(database, DataTypes) {
    var list,
        taxon = database.import('./taxon'),
        user = database.import('./user');

    list =  database.define(
        'List',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            name: DataTypes.STRING,
            description: DataTypes.TEXT,
            private: DataTypes.BOOLEAN,
            notifications: DataTypes.BOOLEAN
        },
        _.extend(commonTableOptions, {
            classMethods: {
                getWhitelist: function () {
                    return ['name', 'description', 'private', 'notifications'];
                }
            }
        })
    );

    user.hasMany(list, {foreignKeyConstraint: true, onDelete: 'cascade'});
    list.belongsTo(user);

    return list;
}
