var commonTableOptions = require('./tableOptions');

module.exports = function(database, DataTypes) {
    return database.define(
        'BiologicalInteraction',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            directional: DataTypes.BOOLEAN,
            name: DataTypes.STRING,
            description: DataTypes.TEXT
        },
        commonTableOptions
    );
}
