module.exports = function (db, success, error) {
    var TaxonAuthor = db.import('./taxonAuthor'),
        DataSource = db.import('./dataSource'),
        Expert = db.import('./expert'),
        GeographicDivision = db.import('./geographicDivision'),
        Jurisdiction = db.import('./jurisdiction'),
        Publication = db.import('./publication'),
        Vernacular = db.import('./vernacular'),
        Hierarchy = db.import('./hierarchy'),
        Taxon = db.import('./taxon'),
        Rank = db.import('./rank'),
        Relationship = db.import('./relationship'),
        BiologicalInteraction = db.import('./biologicalInteraction'),
        User = db.import('./user'),
        List = db.import('./list'),
        ListItem = db.import('./listItem');

    db.sync().success(success).error(error);
}
