var commonTableOptions = require('./tableOptions');

module.exports = function(database, DataTypes) {
    return database.define(
        'Publication', {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            author: DataTypes.STRING(100),
            title: {
                type: DataTypes.STRING(255),
                allowNull: true
            },
            publicationName: DataTypes.STRING(255),
            listedPublicationDate: {
                type: DataTypes.DATE,
                allowNull: true
            },
            actualPublicationDate: DataTypes.DATE,
            publisher: {
                type: DataTypes.STRING(80),
                allowNull: true
            },
            publicationPlace: {
                type: DataTypes.STRING(40),
                allowNull: true
            },
            isbn: {
                type: DataTypes.STRING(16),
                allowNull: true
            },
            issn: {
                type: DataTypes.STRING(16),
                allowNull: true
            },
            pages: {
                type: DataTypes.STRING(15),
                allowNull: true
            },
            comment: {
                type: DataTypes.STRING(500),
                allowNull: true
            }
        },
        commonTableOptions
    );
}
