var commonTableOptions = require('./tableOptions'),
    _ = require('lodash');

module.exports = function(database, DataTypes) {
    var user =  database.define(
        'User',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            name: DataTypes.STRING,
            username: {
                type: DataTypes.STRING,
                unique: true
            },
            email: {
                type: DataTypes.STRING,
                unique: true
            },
            password: DataTypes.STRING,
            confirmationToken: DataTypes.STRING,
            lastLogin: DataTypes.DATE,
            passwordRequestedAt: DataTypes.DATE,
            lastLogin: DataTypes.DATE,
            enabled: DataTypes.BOOLEAN
        },
        _.extend(commonTableOptions, {
            classMethods: {
                getWhitelist: function () {
                    return ['name', 'username', 'email', 'password'];
                }
            }
        })
    );

    // user.hasOne(media, {as: 'image', foreignKey: 'image_id', foreignKeyConstraint: true});

    return user;
}
