var commonTableOptions = require('./tableOptions');

module.exports = function(database, DataTypes) {
    return database.define(
        'DataSource',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            type: DataTypes.STRING(10),
            source: DataTypes.STRING(64),
            version: DataTypes.STRING(10),
            acquiredAt: DataTypes.DATE,
            comment: {
                type: DataTypes.STRING(500),
                allowNull: true
            }
        },
        commonTableOptions
    );
}
