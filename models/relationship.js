var commonTableOptions = require('./tableOptions');

module.exports = function(database, dataTypes) {
    var relationship,
        taxon = database.import('./taxon'),
        biologicalInteraction = database.import('./biologicalInteraction');

    relationship = database.define(
        'Relationship',
        {
            id: {
                type: dataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            title: dataTypes.STRING,
            description: dataTypes.TEXT
        },
        commonTableOptions
    );

    // Add link to taxons. The link is directional. Where a biological interaction
    // requires an actor or initiator, it will always be Taxon1
    taxon.hasMany(relationship, {as: 'Taxon1', foreignKey: 'taxon_1_id', foreignKeyConstraint: true});
    taxon.hasMany(relationship, {as: 'Taxon2', foreignKey: 'taxon_2_id', foreignKeyConstraint: true});

    biologicalInteraction.hasMany(relationship, {foreignKey: 'biological_interaction_id', foreignKeyConstraint: true});

    return relationship;
}
