var commonTableOptions = require('./tableOptions');

module.exports = function(database, DataTypes) {
    return database.define(
        'Vernacular',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            name: DataTypes.STRING(80),
            language: DataTypes.STRING(15),
            approved: DataTypes.BOOLEAN
        },
        commonTableOptions
    );
}
