var commonTableOptions = require('./tableOptions'),
    DAOFactory = require('sequelize/lib/dao-factory'),
    _ = require('lodash');

module.exports = function(database, DataTypes) {
    var listItem,
        taxon = database.import('./taxon'),
        user = database.import('./user'),
        relationship = database.import('./relationship'),
        list = database.import('./list');

    listItem =  database.define(
        'ListItem',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            notes: DataTypes.TEXT,
        },
        _.extend(commonTableOptions, {
            classMethods: {
                getQueryOptions: function () {
                    return {include: [taxon, user, relationship]};
                },
                getWhitelist: function () {
                    return ['notes', 'relationship_id', 'list_id', 'user_id', 'taxon_id'];
                }
            }
        })
    );

    listItem.belongsTo(taxon, {foreignKeyConstraint: true, onDelete: 'cascade', allowNull: true});
    listItem.belongsTo(relationship, {foreignKeyConstraint: true, onDelete: 'cascade', allowNull: true});
    listItem.belongsTo(user, {foreignKeyConstraint: true, onDelete: 'cascade', allowNull: true});

    list.hasMany(listItem);
    listItem.belongsTo(list, {foreignKeyConstraint: true, onDelete: 'cascade', allowNull: false});

    return listItem;
}
