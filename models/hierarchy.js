var commonTableOptions = require('./tableOptions');

module.exports = function(database, DataTypes) {
    return database.define(
        'Hierarchy',
        {
            ancestor_id: {
                type: DataTypes.INTEGER,
                references: "Taxon",
                referencesKey: "id"
            },
            descendant_id: {
                type: DataTypes.INTEGER,
                references: "Taxon",
                referencesKey: "id"
            },
            depth: DataTypes.INTEGER
        },
        commonTableOptions
    );
}
