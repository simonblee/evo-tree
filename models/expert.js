var commonTableOptions = require('./tableOptions');

module.exports = function(database, DataTypes) {
    return database.define(
        'Expert',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            name: DataTypes.STRING(100),
            comment: {
                type: DataTypes.STRING(500),
                allowNull: true
            }
        },
        commonTableOptions
    );
}
