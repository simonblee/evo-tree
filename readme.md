TODO Before Launch:

* User authentication
* Object authorisation public, admin and user
* POSTing objects uses route parameters to set some association in object
* Add query string to URLs to return only specified field from object when performing a GET request (idenx and show)
* Login with Facebook, Twitter, etc
* Check relationships exist when submitting ids in forms

TODO After Launch:

* Application ID for API
* Client token for requests to private objects for API (OAuth)
* Webhooks to POST to URLs when data changes, eg. lists, taxa, etc.
