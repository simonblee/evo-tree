var config = require('../config/config'),
    db = config.get('sequelize'),
    Taxon = db.import('../models/taxon'),
    Rank = db.import('../models/rank'),
    _ = require('lodash');

module.exports.ancestors = function (req, res) {
    var firstTaxon = parseInt(decodeURI(req.params.firstTaxon)),
        secondTaxon = parseInt(decodeURI(req.params.secondTaxon));

    Taxon.findAncestors(firstTaxon, secondTaxon).success(function (taxons) {
        if (! taxons.length) {
            var msg = 'Could not find ancestors.';
            res.status(404).send(msg);
        } else {
            taxons = setTaxonUrls(req, taxons);
            res.json(taxons);
        }
    });
}

/**
 * Find a list of taxons to display in the search along with
 * their rank. TODO: Replace with Apace Lucene/Solr index
 */
module.exports.search = function (req, res) {
    var name = decodeURI(req.params.name).replace(/\%/g, '') + '%',
        limit = req.query.limit ? parseInt(req.query.limit, 10) : 20;

    Taxon.findAll({where: ['Taxon.name LIKE ?', name], limit: limit, include: [Rank]}).success(function (taxons) {
        taxons = setTaxonUrls(req, taxons);
        res.json(taxons);
    });
}

/**
 * Get a single taxon by id or name. Pass in 'true' for 'parent'
 * argument to return the parent of the taxon specified in the
 * request URL.
 */
module.exports.show = function (req, res, parent) {
    var params, query,
        taxonParam = req.params.taxon;

    if (_.isNaN(parseInt(taxonParam, 10))) {
        params = parent ? taxonParam : {where: {name: decodeURI(taxonParam)}};
    } else {
        params = parseInt(taxonParam, 10);
    }

    // Run the query
    if (parent) {
        query = Taxon.findParent(params);
    } else {
        query = Taxon.find(params);
    }

    query.success(function (taxon) {
        if (! taxon) {
            var msg = 'Could not find '+ (parent ? 'parent of' : '') + ' taxon with ' + (_.isNaN(parseInt(taxonParam, 10)) ? 'name ' : 'id ') + taxonParam;
            res.status(404).send(msg);
        } else {
            taxon = setTaxonUrls(req, taxon)[0];
            res.json(taxon);
        }
    }).error(function (err) {queryError(err, res)});
}

/**
 * Get a list of taxon either by a single rank type or as
 * a child rank type below a specific taxon. Examples:
 * /kingdom
 * /taxon/:taxon/species
 */
module.exports.list = function (req, res, directChildren) {
    var query,
        params = {},
        pathParts = req.route.path.split('/');
        parentType = pathParts[1],
        taxon = pathParts[2] ? req.params.taxon : null,
        childType = pathParts[3] || null,
        limit = req.query.limit ? parseInt(req.query.limit, 10) : 20,
        offset = req.query.offset ? parseInt(req.query.offset, 10) : 0;

        // Execute the query
        if (taxon) {
            if (directChildren) {
                query = Taxon.findDirectChildren(taxon, limit, offset);
            } else {
                query = Taxon.findChildren(taxon, childType, limit, offset);
            }
        } else {
            query = Taxon.findByRank(parentType, limit, offset);
        }

    query.success(function (results) {
        results = setTaxonUrls(req, results);
        res.json(results);
    }).error(function (err) {queryError(err, res)});
}

function setTaxonUrls (req, taxons) {
    if (!_.isArray(taxons)) taxons = [taxons];

    return taxons.map(function (taxon) {
        return _.extend(taxon.values || taxon, {
            url: req.router.build('taxon', {taxon: taxon.id}),
            parentUrl: req.router.build('taxon.parent', {taxon: taxon.id}),
            childrenUrl: req.router.build('taxon.children', {taxon: taxon.id}),
        });
    });
}

function queryError (err, res) {
    res.status(500).json('An unknown error occured');
}

function constructModelFromQuery (data) {
    var split,
        model = {};

    for (key in data) {
        split = key.split('_');
        if (split.length > 1) {
            if (!model[split[0]]) model[split[0]] = {};
            model[split[0]][split[1]] = data[key];
        } else {
            model[key] = data[key];
        }
    }

    return model;
}
