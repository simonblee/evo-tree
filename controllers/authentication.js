var passport = require('passport'),
    config = require('../config/config'),
    db = config.get('sequelize'),
    User = db.import('../models/user'),
    LocalStrategy = require('passport-local').Strategy,
    bcrypt = require('bcryptjs');

var STATUS_OK = 200,
    STATUS_NO_CONTENT = 204,
    STATUS_NOT_FOUND = 404,
    STATUS_BAD_REQUEST = 400,
    STATUS_INTERNAL_SERVER_ERROR = 500;

passport.use(new LocalStrategy(
    function (username, password, done) {
        User.find({where: {username: username}}).success(function (user) {
            if (user) {
                bcrypt.compare(password, user.password, function (err, res) {
                    done(null, res ? user : false);
                });
            } else {
                done(null, false);
            }
        }).error(function (err) {
            done(err);
        });
    }
));

// TODO: Serialize/deserialize entire object into/out of Redis. Updates to User
// object (eg. user editing their profile) should be reflected in the session too.

// Serialize/deserialize user from the session
passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.find(id).success(function (user) {
        done(null, user);
    }).error(function (err) {
        done(err, null);
    });
});

/**
 * Login handled by passport. Login success handled here. Passport returns 401
 * HTTP error on login failure.
 */
module.exports.loginSuccess = function (req, res) {
    res.status(STATUS_OK).send();
}

module.exports.logout = function (req, res) {
    req.logout();
    res.status(STATUS_OK).send();
}

module.exports.resetRequest = function (req, res) {

}

module.exports.reset = function (req, res) {

}
