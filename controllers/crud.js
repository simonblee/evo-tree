var Controller,
    config = require('../config/config'),
    db = config.get('sequelize'),
    formHelper = require('../helper/form'),
    _ = require('lodash'),
    changeCase = require('change-case'),
    EventEmitter = require('events').EventEmitter;

// HTTP Status codes
var STATUS_OK = 200,
    STATUS_NO_CONTENT = 204,
    STATUS_NOT_FOUND = 404,
    STATUS_BAD_REQUEST = 400,
    STATUS_INTERNAL_SERVER_ERROR = 500;

/**
 * Generic controller with CRUD actions. Provides index (list), show,
 * update, create, and delete actions. Strict rules apply to route parameters.
 * Associations to a model in the route must be the same as the association
 * id in the model. eg. user_id for a user association. The id of the the
 * model must be 'id'.
 */
module.exports = Controller = function (type) {
    this.modelType = type;
    this.modelClass = db.import('../models/' + type);
    EventEmitter.call(this);
}

/**
 * Make controller an event emitter
 */
Controller.prototype = _.extend(Controller.prototype, EventEmitter.prototype);

/**
 * Authorise the user to perform action.
 */
Controller.prototype.authorise = function (req, res) {

}

/**
 * Return a list of models in the response. Will automatically
 * add all route parameters to the query's where clause.
 */
Controller.prototype.index = function (req, res) {
    var self = this,
        limit = req.query.limit ? parseInt(req.query.limit, 10) : 20,
        offset = req.query.offset ? parseInt(req.query.offset, 10) : 0,
        where = getWhereClauseFromRequest(req),
        commonParams = this.modelClass.getQueryOptions ? this.modelClass.getQueryOptions() : {},
        params = {
            limit: limit,
            offset: offset
        };

    if (where) params.where = where;
    params = _.extend(params, commonParams);

    this.emit('before:index', req, res);

    this.modelClass.findAll(params).success(function (models) {
        models = models.map(function (model) {
            return self.setShowUrl(model, req.router, req.params);
        });
        self.emit('after:index', req, res, models);
        res.json(models);
    });
}

Controller.prototype.show = function (req, res) {
    var self = this;

    this.emit('before:show', req, res);

    this.findFromRequest(req, res, function (model) {
        model = self.renderModelResponse.call(self, model, req, res);
        self.emit('after:show', req, res, model);
    });
}

Controller.prototype.create = function (req, res) {
    var model,
        self = this;

    if (validateFormParameters(req, res, this.modelClass.getWhitelist())) {
        this.emit('before:create', req, res);

        this.modelClass.create(req.body).success(function (model) {
            // Find again to return any joins specified in the model class
            self.find({where: {id: model.id}}, res, function (model) {
                // Generate model url and send response
                model = self.renderModelResponse.call(self, model, req, res);
                self.emit('after:create', req, res, model);
            });
        }).error(function (err) {
            validationErrorResponse(err, req, res);
        });
    }
}

Controller.prototype.update = function (req, res) {
    var self = this;

    if (validateFormParameters(req, res, this.modelClass.getWhitelist())) {
        this.emit('before:update', req, res);

        this.findFromRequest(req, res, function (model) {
            model.updateAttributes(req.body).success(function (model) {
                model = self.renderModelResponse.call(self, model, req, res);
                self.emit('after:update', req, res, model);
            }).error(function (err) {
                validationErrorResponse(err, req, res);
            });
        });
    }
}

Controller.prototype.delete = function (req, res) {
    var self = this;

    this.emit('before:delete', req, res);

    this.modelClass.destroy({id: req.params.id}).success(function (rows) {
        self.emit('after:delete', req, res, rows);
        res.status(STATUS_NO_CONTENT).send();
    }).error(function (err) {
        res.status(STATUS_INTERNAL_SERVER_ERROR).send();
    });
}

/**
 * Generate a url for the model. Use the id from the model and
 * the parameters from the existing request.
 */
Controller.prototype.setShowUrl = function (model, router, requestParams) {
    var routeName = changeCase.snakeCase(this.modelType) + '_show';
    requestParams.id = model.values.id;

    return _.extend(model.values, {url: router.build(routeName, requestParams)});
}

/**
 * Find a model in the database. Return 404 if not found.
 *
 * TODO: User parameters other than id to find the model
 */
Controller.prototype.findFromRequest = function (req, res, done) {
    var options = {where: getWhereClauseFromRequest(req)};
    this.find(options, res, done);
}

Controller.prototype.find = function (options, res, done) {
    var self = this,
        commonOptions = this.modelClass.getQueryOptions ? this.modelClass.getQueryOptions() : {};

    this.modelClass.find(_.extend(options, commonOptions)).success(function (model) {
        if (model) done(model);
        else {
            res.status(STATUS_NOT_FOUND).send(self.modelType + ' not found.');
        }
    });
}

/**
 * Return a model as the response
 */
Controller.prototype.renderModelResponse = function (model, req, res) {
    model = this.setShowUrl(model, req.router, req.params);
    res.json(model);

    return model;
}

function getWhereClauseFromRequest (req) {
    var param,
        where = _.clone(req.params);

    delete where._masked;

    if (! _.keys(where).length) return null;
    return where;
}

function validationErrorResponse (err, req, res) {
    res.status(STATUS_BAD_REQUEST).json(err);
}

function validateFormParameters (req, res, whitelist) {
    if (! formHelper.validateParameters(req, whitelist)) {
        res.status(STATUS_BAD_REQUEST).send('Invalid model parameters. Only ' + whitelist.join(',') + ' are allowed.');
        return false;
    }

    return true;
}
