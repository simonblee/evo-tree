var controller = require('../controllers/taxon'),
    sequelize = require('../config/config').get('sequelize'),
    Rank = sequelize.import('../models/rank');

/**
 * Callback after routes are set up
 */
module.exports.setup = function (app, afterRouteSetupCallback) {

    // Find common ancestors for two or single taxa
    app.get('/taxon/ancestors/:firstTaxon/:secondTaxon', 'taxon.ancestors', controller.ancestors);
    app.get('/taxon/ancestors/:firstTaxon', 'taxon.ancestors', controller.ancestors);

    // Search for a taxon
    app.get('/taxon/search/:name', 'taxon.search', controller.search);

    // Find a specific taxon by id or name
    app.get('/taxon/:taxon', 'taxon', function (req, res) {
        controller.show(req, res, false);
    });

    // List DIRECT children of taxon
    app.get('/taxon/:taxon/children', 'taxon.children', function (req, res) {
        controller.list(req, res, true);
    });

    // Get the DIRECT parent of taxon
    app.get('/taxon/:taxon/parent', 'taxon.parent', function (req, res) {
        controller.show(req, res, true);
    });

    // Allow finding of a list of taxa in a particular rank or restrict the rank
    // to a parent rank.
    //
    // Items to the right may only be children of items to their left. Example routes:
    //
    // /domain/:domain/kingdom
    // /domain/:domain/phylum
    // /phylum/:phylum/class
    // /phylum/:phylum/order
    //
    // Pull ranks from the database and order by their 'level' in the rank hierarchy
    Rank.findAll({order: '`level` ASC'}).success(function(ranks) {
        ranks.forEach(function (rank, index, ranks) {
            var route;

            // List rank type without parent filter
            app.get('/'+rank.name.toLowerCase(), controller.list);

            // List rank type with parent filter (for all types below parent level)
            ranks.slice(index + 1).forEach(function (childRank, idx, childNodes) {
                if (childRank.level <= rank.level) return;
                route = '/taxon/:taxon/'+childRank.name.toLowerCase();
                app.get(route, controller.list);
            });
        });

        if (afterRouteSetupCallback) afterRouteSetupCallback();
    }).error(function (error) {
        console.log(error);
    });
}
