var passport = require('passport'),
    authenticationController = require('../controllers/authentication');

module.exports.setup = function (app, afterRouteSetupCallback) {
    app.post('/login', 'login', passport.authenticate('local'), authenticationController.loginSuccess);
    app.get('/logout', 'logout', authenticationController.logout);
    app.get('/password-reset-request', 'password_reset_request', authenticationController.resetRequest);
    app.post('/password-reset-request', 'password_reset_request_submit', authenticationController.resetRequest);
    app.get('/password-reset', 'password_reset', authenticationController.reset);
    app.post('/password-reset', 'password_reset_submit', authenticationController.reset);
}
