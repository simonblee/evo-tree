var CrudController = require('../controllers/crud'),
    Auth = require('../helper/auth'),
    _ = require('lodash'),
    config = require('../config/config'),
    acl = config.get('acl');

// TODO: Split this up into files by object type.
// Split permissions into separate files too.
module.exports.setup = function (app, afterRouteSetupCallback) {
    var userController = new CrudController('user'),
        listController = new CrudController('list'),
        listItemController = new CrudController('listItem'),

        userIndex = _.bind(userController.index, userController),
        userShow = _.bind(userController.show, userController),
        userUpdate = _.bind(userController.update, userController),
        userDelete = _.bind(userController.delete, userController),

        listIndex = _.bind(listController.index, listController),
        listShow = _.bind(listController.show, listController),
        listUpdate = _.bind(listController.update, listController),
        listCreate = _.bind(listController.create, listController),
        listDelete = _.bind(listController.delete, listController),

        listItemIndex = _.bind(listItemController.index, listItemController),
        listItemShow = _.bind(listItemController.show, listItemController),
        listItemUpdate = _.bind(listItemController.update, listItemController),
        listItemCreate = _.bind(listItemController.create, listItemController),
        listItemDelete = _.bind(listItemController.delete, listItemController);

    // Fetch all models defined in the URL before
    // any route is matched
    app.use(function (req, res, next) {
        console.log('%s %s', req.method, req.url);
        next();
    });

    // User routes
    app.get('/users', 'user_index', Auth.authorise(), userIndex);
    app.get('/users/:id', 'user_show', Auth.authorise(), userShow);
    app.put('/users/:id', 'user_update', Auth.authorise(true), userUpdate);
    app.delete('/users/:id', 'user_delete', Auth.authorise(true), userDelete);

    // ==== General ACL Config
    // TODO: Move
    acl.addUserRoles('guest', 'guest'); // 'guest' is used as a user id and a role; add the role 'guest' to the user id 'guest'
    acl.addRoleParents('user', 'admin'); // set up role hierarchy
    // ====

    acl.allow(['guest', 'user'], '/users', 'get');

    userController.on('after:create', function (req, res, model) {
        var role = 'user_' + req.user.id.toString(),
            listIndexUrl = req.router.build('list_index', {user_id: model.id}),
            listCreateUrl = req.router.build('list_create', {user_id: model.id}),
            readRoles = ['guest', 'user'],
            writeRoles = [role, 'admin'];

        // Add roles to the user
        acl.addUserRoles(req.user.id.toString(), ['user', role]);

        // Access to the user
        acl.allow(writeRoles, model.url, ['put', 'delete']);
        acl.allow(readRoles, model.url, 'get');

        // Access to user's subordinate objects
        acl.allow(readRoles, listIndexUrl, 'get');
        acl.allow(writeRoles, listCreateUrl, 'post');
    });

    userController.on('after:delete', function (req, res, rows) {
        acl.removeResource(req.url);
        acl.removeResource(req.router.build('list_index', {user_id: req.params.id}));
        acl.removeResource(req.router.build('list_create', {user_id: req.params.id}));
    });

    // List routes
    app.get('/users/:user_id/lists', 'list_index', Auth.authorise(), listIndex);
    app.get('/lists/:id', 'list_show', Auth.authorise(), listShow); // Lists may be private, so authorise
    app.put('/lists/:id', 'list_update', Auth.authorise(true), listUpdate);
    app.post('/users/:user_id/lists', 'list_create', Auth.authorise(true), listCreate);
    app.delete('/lists/:id', 'list_delete', Auth.authorise(true), listDelete);

    listController.on('after:create', function (req, res, model) {
        var roles = ['user_' + req.user.id.toString(), 'admin'],
            listItemCreateUrl = req.router.build('list_item_create', {list_id: model.id}),
            listItemIndexUrl = req.router.build('list_item_index', {list_id: model.id});

        acl.allow(roles, model.url, ['get', 'put', 'delete']);
        acl.allow(roles, listItemCreateUrl, 'post');
        acl.allow(roles, listItemIndexUrl, 'get');
    });

    listController.on('after:create', createUpdateListAcl);
    listController.on('after:update', createUpdateListAcl);

    listController.on('after:delete', function (req, res) {
        acl.removeResource(req.url);
        acl.removeResource(req.router.build('list_item_index', {list_id: model.id}));
        acl.removeResource(req.router.build('list_item_create', {list_id: model.id}));
    });

    /**
     * Handle setting a list to private or public.
     */
    function createUpdateListAcl (req, res, model) {
        var fn = model.values.private ? 'removeAllow' : 'allow',
            listItemIndexUrl = req.router.build('list_item_index', {list_id: model.id});

        // Read access to the list and list items
        acl[fn](['guest', 'user'], model.url, 'get');
        acl[fn](['guest', 'user'], listItemIndexUrl, 'get');
    }

    // List items - link between a list and user/taxon/relationship/...etc
    app.get('/lists/:list_id/items', 'list_item_index', Auth.authorise(), listItemIndex);
    app.get('/lists/:list_id/items/:id', 'list_item_show', Auth.authorise(), listItemShow);
    app.put('/lists/:list_id/items/:id', 'list_item_update', Auth.authorise(true), listItemUpdate);
    app.post('/lists/:list_id/items', 'list_item_create', Auth.authorise(true), listItemCreate);
    app.delete('/lists/:list_id/items/:id', 'list_item_delete', Auth.authorise(true), listItemDelete);

    // TODO: User permissions from parent list
    listItemController.on('after:create', function (req, res, model) {});
    listItemController.on('after:delete', function (req, res) {});

    if (afterRouteSetupCallback) afterRouteSetupCallback();
}
